var gulp = require('gulp');
var less = require('gulp-less');
var filter = require('gulp-filter');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var autoprefixer = require('gulp-autoprefixer');
var include = require('gulp-include');
var cssmin = require('gulp-cssmin');
var uncss = require('gulp-uncss');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var jsmin = require('gulp-jsmin');



gulp.task('less:dev', function () {
    gulp.src('./src/less/style.less')
        .pipe(less().on('error',function(e){console.log(e)})) //Скомпилируем
        //.pipe(sourcemaps.init({loadMaps: true}))
        .pipe(filter('*.css'))
       /* .pipe(uncss({
            "ignore": [
                ".fixed-position"
            ],
            html: ['./src/tmpl/!**!/!*.html']
        }))*/
        .pipe(autoprefixer({
            browsers: ['last 4 versions'],
            cascade: true
        }))
        .pipe(cssmin())
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest('./web/css'))
        .pipe(reload({ stream:true }));
});


gulp.task('html:dev', function () {

    gulp.src('./src/tmpl/*.html') //for localhost workflow
        .pipe(include())
        .pipe(gulp.dest("./web"));

   /* gulp.src('./src/tmpl/index_render.php') //server side workflow
        .pipe(include())
        .pipe(gulp.dest("./web"));*/

});

gulp.task('js:dev',function(){
    gulp.src('./src/js/**/*')
        .pipe(gulp.dest("./web/js"));
});

gulp.task('media:dev',function(){
    gulp.src('./src/fonts/**/*')
        .pipe(gulp.dest("./web/fonts"));
    gulp.src('./src/images/**/*')
        .pipe(gulp.dest("./web/images"));
});

gulp.task('browser-sync', function () {
    var historyApiFallback = require('connect-history-api-fallback');
    //  console.log(historyApiFallback);
    browserSync({
        server: {
            baseDir: "./web",
            middleware: [historyApiFallback]
        }
    });
});

gulp.task('bs_reload', function () {
    browserSync.reload();
});

gulp.task('clear-web-dir', function () {
    gulp.src(['./web/**/*']).pipe(clean({force: true}));
});



gulp.task('dev-watch', ['less:dev', 'html:dev', 'js:dev', 'media:dev' , 'browser-sync'], function () {

    gulp.watch(["./src/tmpl/**/*"], ['html:dev']);
    gulp.watch("./src/less/**/*", ['less:dev']);
    gulp.watch("./src/js/**/*", ['js:dev']);
    gulp.watch("./src/media/**/*", ['media:dev']);

    gulp.watch(["./src/js/**/*", "./src/tmpl/**/*","./src/media/**/*"], ['bs_reload']);

});


// Функция открытия окна выбора города
$('.b-select-city_current').on('click', function (event) {
    var $self = $(this),
        $parent = $self.parent();
    $parent.toggleClass('_offices');
    event.stopPropagation();
});

$(document).on("click", function (event) {
    if ($(event.target).closest(".b-select-offices").length)
        return;

    if ($(".b-select-city").hasClass("_offices")) {
        $(".b-select-city").removeClass("_offices");
    }
    event.stopPropagation();
});
//    select city

$(document).on('click', '._offices .b-select-offices__name a', function (event) {
    var that = $(this);
    var block = that.parent().parent().parent().parent().find(".b-select-city");
    block.removeClass("_offices").addClass("_city");
    event.stopPropagation();
});

$(".b-select-city_item", ".b-select-city").on('click', function (event) {
    var that = $(this);
    that.parent().parent().parent().parent().parent().find(".b-select-city").addClass("_offices").removeClass("_city");
    var $city = $(this).text();
//    that.parent().parent().parent().parent().parent().find("a", ".b-select-offices__name").text($city);
    that.parent().parent().parent().parent().parent().find(".b-select-offices__name").text($city);
    event.stopPropagation();
});

$(document).on('click', '.b-select-offices .close',function (event) {
    var that = $(this);
    that.parent().parent().parent().parent().find('.b-select-city').removeClass('_offices');

    event.stopPropagation();
});

///* script of this page start

$(document).on('click' , '.navigation a', function(){

    var that = $(this);
    var bar = that.parent().parent().find('.service-bar');
    var left = Math.abs(parseInt(bar.css('left')));
    var oneBlock = bar.find('.block').eq(0).width();
    var current = Math.abs(Math.ceil(left / oneBlock));
    var max = bar.find('.block').size() - 3;
    var offset = 0;

    if (that.hasClass('prev')) {
        if (current == 0) {
            current = max;
        } else {
            current = current - 1;
        }
    } else {
        if (current == max) {
            current = 0;
        } else {
            current = current + 1;
        }
    }

    current = current * oneBlock;

    bar.stop().animate({
        left: -current
    }, 500);

})

var i = 0;
$(window).scroll(function() {
    var height = $(window).scrollTop();
    if($('.services-block-two').size() > 0) {
        var elem = $('.services-block-two').offset().top;
        var line = $('.orange-line');
        var blocks = $('.services-block-three-list-block');

        if(height  > (elem + $('.services-block-two').height() / 2)&& i == 0) {
            i++;
            // do something
            timeout = setTimeout(function(){
                line.animate({
                    'width': '42%'
                },100,function(){
                    blocks.eq(1).animate({
                        opacity: 1
                    } , 1100)
                });
                blocks.eq(0).animate({
                    opacity: 1
                } , 500);

            }, 10);


            timeout = setTimeout(function(){

                line.animate({
                    'width': '100%'
                },100,function(){
                    blocks.eq(2).animate({
                        opacity: 1
                    } , 1000);

                });

            }, 1000);
        }
    }
});

function resizeSlider(){
    var sliderOne = $('.service-wrapper-mobile');
    var oneBlockOne = Math.ceil(sliderOne.find('.block').eq(0).width());
    var sliderBarOne = sliderOne.find('.service-bar').eq(0);
    var offsetOne = Math.floor(Math.abs(parseInt(sliderBarOne.css('left'))) / oneBlockOne);
    sliderBarOne.css({
        'left' : -offsetOne*oneBlockOne
    });


    var sliderOne = $('.services-wrapper');
    var oneBlockOne = Math.ceil(sliderOne.find('.block').eq(0).width());
    var sliderBarOne = sliderOne.find('.service-bar');
    var offsetOne = Math.floor(Math.abs(parseInt(sliderBarOne.css('left'))) / oneBlockOne);
    sliderBarOne.css({
        'left' : -offsetOne*oneBlockOne
    });

}
$( window ).resize(function() {
    resizeSlider();
});
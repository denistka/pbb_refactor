$(document).on('click' , '.left-menu-item' , function(){

    var that = $(this);
    var activeClass = 'active';
    var menuItems = $('.left-menu-item');
    var menuItemBlocks = $('.left-menu-item-block');

    if(!that.hasClass(activeClass)) {
        menuItemBlocks.removeClass(activeClass).slideUp();
        menuItems.removeClass(activeClass);
        that.addClass(activeClass);
        that.next().addClass(activeClass).slideDown();
    } else {
        that.removeClass(activeClass);
        that.next().removeClass(activeClass).slideUp();
    }


}).on('click' , '.left-menu-item-block .checker' , function(){


    var that = $(this);
    var activeClass = 'active';
    var checkers = $('.left-menu-item-block .checker');

    if(!that.hasClass(activeClass)) {
        //checkers.removeClass(activeClass);
        that.addClass(activeClass);
    } else {
        that.removeClass(activeClass);
    }



}).on('click' , '.left-menu-item-block .return-to-list' , function(){


    var list = $('.left-menu-item-block .offices-list');
    var desc = $('.left-menu-item-block .offices-desc');
    var time = 500;

    desc.find('.mini-map').html('').attr('style', '');

    desc.slideUp();
    setTimeout(function(){
        list.slideDown(time);
    }, time);


    return false;


}).on('click' , '.left-menu-item-block .one-office' , function(){


    var list = $('.left-menu-item-block .offices-list');
    var desc = $('.left-menu-item-block .offices-desc');
    var time = 500;

    list.slideUp(time);
    setTimeout(function(){
        desc.slideDown('slow' , function(){

            desc.find('.mini-map').velocity('stop').velocity('transition.expandIn', {
                duration: 300,
                complete: function(){
                    mapInit();
                }
            });
        });
    }, time);
    $('.right-vacancy-block .big-map-wrapper .close-btn').trigger('click');
    return false;


}).on('click' , '.left-menu-item-block .look-at-map' , function(){

    var map_wrap = $('.big-map-wrapper');
    var time = 500;
    var className = 'open';

    if(!map_wrap.hasClass(className)) {
        customScrollTo(map_wrap.parent() ,time, function(){
            map_wrap.velocity('stop').velocity('transition.slideDownIn', {
                duration: time,
                complete : function(){
                    mapInit();
                    map_wrap.addClass(className);
                }
            });

        });
    }

    return false;



}).on('click' , '.right-vacancy-block .big-map-wrapper .close-btn' , function(){

    var map_wrap = $('.big-map-wrapper');
    var time = 500;
    var className = 'open';

    customScrollTo(map_wrap, time ,function(){
        map_wrap.velocity('stop').velocity('transition.slideUpOut', {
            duration: time,
            complete : function(){
                map_wrap.removeClass(className);
            }
        });
    });


}).on('click' , '.right-vacancy-block .one-vacancy .resume-btn' , function(){

    var that = $(this);
    var parent = that.parent().parent().parent();
    var time = 500;
    var info_block = parent.find('.one-vacancy-inf');
    var resume_from = parent.find('.resume-form-block');

    info_block.slideUp(time);

    customScrollTo(parent, time ,function(){
        resume_from.slideDown(time);
    });

}).on('click' , '.left-menu-item-block .category' , function(){


    var that = $(this);
    var activeClass = 'active';
    var checkers = $('.left-menu-item-block .category');

    if(!that.hasClass(activeClass)) {
        checkers.removeClass(activeClass);
        that.addClass(activeClass);
    }



}).on('click' , '.right-vacancy-block .return-to-desc' , function(){

    var that = $(this);
    var time = 500;
    var parent = that.parent().parent().parent().parent();
    var info_block = parent.find('.one-vacancy-inf');
    var resume_from = parent.find('.resume-form-block');
    resume_from.slideUp(time);


    customScrollTo(parent, time ,function(){
        info_block.slideDown(time);
    });

    return false;

}).on('click' , '.right-vacancy-block .resume-form-send-btn' , function(){

    var that = $(this);
    var time = 500;
    var parent = that.parent().parent().parent().parent();
    var success = parent.find('.resume-success');
    var resume_from = parent.find('.resume-form-block');
    resume_from.slideUp(time);

    customScrollTo(parent, time ,function(){
        success.slideDown(time);
    });

    return false;

}).on('click' , '.right-vacancy-block .one-vacancy-btn' , function(){


    var that = $(this);
    var activeClass = 'open';
    var parent = that.parent();
    var blocks = $('.one-vacancy');
    var hidden_blocks = $('.one-vacancy-hidden');
    var hidden_block = parent.find('.one-vacancy-hidden');

    if($('.resume-form-block').size() > 0) {
        var time = 300;
        var info_block = $('.one-vacancy-inf');
        var success = $('.resume-success');
        var resume_from = $('.resume-form-block');
            resume_from.slideUp(time);
            success.slideUp(time);
            setTimeout(function(){
                info_block.slideDown(time);
            }, time);

    }
    if(!parent.hasClass(activeClass)) {
        customScrollTo(parent, time , function(){
            hidden_blocks.slideUp();
            blocks.removeClass(activeClass);
            parent.addClass(activeClass);
            hidden_block.slideDown();
        })

    } else {
        customScrollTo(parent, time , function(){
            hidden_block.slideUp();
            parent.removeClass(activeClass);
        })
    }



});

$(document).ready(function(){
    $('.resume-phone').mask('+7 (000) - 000 - 00 - 00');
});

function customScrollTo(elem, time , callback){
    var menuHeight, offset;
    if($('.b-main-nav.fixed-position').size() > 0) {
        menuHeight = parseInt($('.b-main-nav').css('height'));
    } else {
        menuHeight = parseInt($('.b-main-nav').css('height')) * 2;
    }

    callback();
    setTimeout(function(){
        offset = parseInt(elem.offset().top - menuHeight);
        $('html, body').animate({
            scrollTop: offset
        }, time);
    },time * 2);
}

function mapInit() {

//map setting
    var zoomValue = 17;
    var canvasBigMap = document.getElementById('vacancyBigMap');
    var canvasSmallMap = document.getElementById('vacancySmallMap');
    var myLatLng = new google.maps.LatLng(55.7548683,37.6344741);

    var styles =[
        {
            featureType: "all",
            elementType: "all",
            stylers: [
                { saturation: -100 } // <-- THIS
            ]
        }
    ];

    var mapOptions = {
        center: myLatLng,
        zoom: zoomValue,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
        styles: styles
    };

    var bigMap = new google.maps.Map(canvasBigMap, mapOptions);
    var smallMap = new google.maps.Map(canvasSmallMap, mapOptions);

    var markerBigMap = new google.maps.Marker({
        position: myLatLng,
        map: bigMap,
        icon: 'images/ofices-map.png',
        title:"Hello World!"
    });

    var markerSmallMap = new google.maps.Marker({
        position: myLatLng,
        map: smallMap,
        icon: 'images/ofices-map.png',
        title:"Hello World!"
    });

    google.maps.event.addListener(markerBigMap, 'mouseover', function() {
        markerBigMap.setIcon('images/ofices-map-hover.png');
    });

    google.maps.event.addListener(markerBigMap, 'mouseout', function() {
        markerBigMap.setIcon('images/ofices-map.png');
    });

    google.maps.event.addListener(markerSmallMap, 'mouseover', function() {
        markerSmallMap.setIcon('images/ofices-map-hover.png');
    });

    google.maps.event.addListener(markerSmallMap, 'mouseout', function() {
        markerSmallMap.setIcon('images/ofices-map.png');
    });

    google.maps.event.trigger(bigMap, 'resize');
    google.maps.event.trigger(smallMap, 'resize');
}
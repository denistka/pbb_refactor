var locations = [
    ['1', 55.771098, 37.620356, 'Цветной бульвар 15, стр. 1', '+7 (495) 73 – 777 – 73'],
    ['2', 55.861064, 37.585520, 'Алтуфьевское шоссе, 22Б', '8-800-2000-790'],
    ['2', 55.772463, 37.638282, 'Большая Сухаревская пл., 9', '8-800-2000-790'],
    ['2', 55.783790, 37.874973, 'МО, г. Балашиха, Микрорайон ЦОВБ, 19', '8-800-2000-790'],
    ['2', 55.712608, 37.376953, 'Можайское шоссе, ул.Амбулаторная, 49А', '8-800-2000-790'],
    ['2', 47.107781, 37.665989, 'Олимпийский пр., д. 16, стр. 5', '8-800-2000-790'],
    ['2', 55.774509, 37.587035, 'ул. 1-я Тверская-Ямская, 21', '8-800-2000-790'],
    ['2', 55.753093, 37.606437, 'ул. Воздвиженка, 10', '8-800-2000-790'],
    ['2', 55.767232, 37.600886, 'ул. Тверская, 22', '8-800-2000-790'],
    ['2', 47.554074, 34.397314, 'ул.Херсонская, д.43', '8-800-2000-790'],
    ['3', 55.738275, 37.614678, 'МО, 23-й км а/д «Балтия», стр. 1, ТЦ «Премьер»', '+7 (495) 988-14-04'],
    ['3', 55.738275, 37.614678, 'ул. Большая Якиманка, д.22', '+7 (495) 995-21-70'],
    ['3', 55.731695, 37.487466, 'Кутузовский проспект, д.48', '+7 (495) 648-18-61'],
    ['3', 55.761058, 37.619627, 'ул. Петровка, д.2, стр.2', '+7 (495) 518-96-60'],
    ['3', 55.81063469999999, 37.50637240000003, 'Ленинградское шоссе, д.112', '+7 (495) 225-87-58'],
    ['3', 55.757785, 37.639422, 'ул. Покровка, д.2/1, стр.1', '+7 (495) 662-66-03'],
    ['4', 55.738230, 37.262542, 'МО, Рублево-Успенское шоссе, деревня Жуковка, д.186', '+7 (495) 662-82-30'],
    ['4', 55.770146, 37.681111, 'Посланников Пер 9/3', '+ 7 495 589 18 81 ext. 224']
];

var style = [
    {
        "featureType": "road",
        "stylers"    : [
            {"color": "#d2d2d2"}
        ]
    },
    {
        "featureType": "landscape.natural",
        "stylers"    : [
            {"color": "#ebebeb"}
        ]
    },
    {
        featureType: "poi.park",
        stylers    : [
            {visibility: "off"}
        ]
    }
]
var boxText = document.createElement("div");
boxText.style.cssText = "border: 1px solid black; margin-top: -110px;text-align: center;background-color: #2e2e2e;padding: 20px;color: white;font-family: PTSansBold;position: relative;";
boxText.innerHTML = "<div style='background: url(" + baseUrl + "/images/black-romb.png)  no-repeat center bottom;position: absolute;  width:100%;top: 36px;height: 30px;margin-left: -20px;'></div>";
boxText.innerHTML += "ул. К. Либкнехта, д. 22, оф. 501";
var boxText2 = document.createElement("div");
boxText2.style.cssText = "border: 1px solid black; margin-top: -200px;text-align: center;background-color: #2e2e2e;padding: 20px;color: white;font-family: PTSansBold;position: relative;";
boxText2.innerHTML = "<div style='background: url(" + baseUrl + "/images/black-romb.png)  no-repeat center bottom;position: absolute;  width:100%;top: 87%;height: 30px;margin-left: -20px;'></div>";
boxText2.innerHTML += "<span style='width: 100%;display: block;'>FABERLIC</span>";
boxText2.innerHTML += "<span style='width: 100%;display: block;color: #929292;margin-top: 10px;margin-bottom: 10px;'>Скидка 10-20% в зависимости<br>от типа карты</span>";
boxText2.innerHTML += "<a href='javascript: return false' onclick='showPartner();' class='btn btn_brown more-map-btn'>подробнее</a>";
var settings = {
    center          : {
        lat: 55.771098,
        lng: 37.620356
    },
    zoom            : 10,
    icon            : baseUrl + 'images/marker-icon.png',
    arrayMarkers    : locations,
    disableDefaultUI: true,
    mapTypeControl  : false,
    styles          : style,
    appendBlock     : boxText2

}
var settings2 = {
    wrapper         : 'calendar-map',
    center          : {
        lat: 55.771098,
        lng: 37.620356
    },
    zoom            : 10,
    icon            : baseUrl + 'images/dark-marker-icon.png',
    arrayMarkers    : locations,
    disableDefaultUI: true,
    mapTypeControl  : false,
    appendBlock     : boxText,
    styles          : style

}
var mapG = new Map(settings);
var mapG2 = new Map(settings2);
mapG.init();
mapG2.init();
$(document).on("click", function (event) {
    if ($(event.target).closest(".b-select-offices").length ||
        $(event.target).closest(".b-select-city_cnt").length ) {
        return;
    }
    if ($(".b-select-city").hasClass("_offices")) {
        $(".b-select-city").removeClass("_offices");
    }

    if ($(".b-select-city").hasClass("_city")) {
        $(".b-select-city").removeClass("_city");
    }
    event.stopPropagation();
})


$(document).on('click', '.b-select-offices .close, .b-select-city_cnt .close', function (event) {
    $('.b-select-city').removeClass('_offices');
    $(".b-select-city").removeClass("_city");

    event.stopPropagation();
});

//    office popup

$(document).on('click', '.b-top-line_offices_popup .close, .b-top-line_offices_popup-overlay, .b-select-offices__item', function (event) {
    $('body').removeClass('offices-active');
    event.stopPropagation();
});

$(document).on('click', '.b-select-offices__item', function (event) {
    var $office = $(this).find(".b-select-offices__title").text(),
        $address = $(this).find(".b-select-offices__address").text();

    $(".b-widget-contacts_caption_name a").text($office);
    $(".b-widget-contacts__address").text($address);

    $(".b-select-city").removeClass("_offices");
    event.stopPropagation();
});

$(".b-select-offices__name a", ".b-top-line_offices_popup").on("click", function (event) {
    $("body").removeClass("offices-active").addClass("city-active");
    event.stopPropagation();
});

//    city popup

$(document).on('click', '.b-top-line_city_popup .close, .b-top-line_city_popup-overlay', function (event) {
    $('body').removeClass('city-active');
    event.stopPropagation();
});

$(".b-select-city_item", ".b-top-line_city_popup").on("click", function (event) {
    $("body").removeClass("city-active").addClass("offices-active");
    var $city = $(this).text();
    $("a", ".b-select-offices__name").text($city);
    event.stopPropagation();
});


//    select city


$(document).on('click', '.b-select-offices .close',function (event) {
    var that = $(this);
    that.parent().parent().parent().parent().find('.b-select-city').removeClass('_offices');

    event.stopPropagation();
}).on('click', '.event .day',function () {

    var that = $(this).parent();
    var bar = $('.calendar-bar');
    var currentBlock = that.index();
    var left = parseInt(bar.css('left'));
    var offset = 0;
    $('.event').removeClass('current');
    that.addClass('current');
    for (var i = 0; i < currentBlock; i++) {
        offset += parseInt($('.event').eq(i).width());

    }
    var oldMonth = $('.calendar-month.active').attr('data');
    var oldYear = parseInt($('.calendar-year').text());
    var newMonth = $('.event').eq(currentBlock).attr('data-m');
    var newYear = $('.event').eq(currentBlock).attr('data-y');
    if (oldMonth != newMonth) {
        $('.calendar-month').removeClass('active');
        $('.calendar-links').find('[data= "' + newMonth + '"]').addClass('active');
    }
    if (oldYear != newYear) {
        $('.calendar-year').text(newYear);
    }
    offset -= $('.calendar').offset().left;
    bar.stop().animate({
        left: -offset
    }, 500);

}).on('click', '.nav-events a',function () {

    var that = $(this);
    var bar = $('.calendar-bar');
    var left = parseInt(bar.css('left'));
    var current = $('.event.current').index();
    var max = $('.event').size() - 1;
    var offset = 0;
    $('.event').removeClass('current');

    if (that.hasClass('prev')) {
        if (current == 0) {
            current = max;
        } else {
            current = current - 1;
        }
    } else {
        if (current == max) {
            current = 0;
        } else {
            current = current + 1;
        }
    }

    var oldMonth = $('.calendar-month.active').attr('data');
    var oldYear = parseInt($('.calendar-year').text());
    var newMonth = $('.event').eq(current).attr('data-m');
    var newYear = $('.event').eq(current).attr('data-y');
    if (oldMonth != newMonth) {
        $('.calendar-month').removeClass('active');
        $('.calendar-links').find('[data= "' + newMonth + '"]').addClass('active');
    }
    if (oldYear != newYear) {
        $('.calendar-year').text(newYear);
    }
    offset -= $('.calendar').offset().left;
    $('.event').eq(current).addClass('current');
    for (var i = 0; i < current; i++) {
        offset += parseInt($('.event').eq(i).width());

    }

    bar.stop().animate({
        left: -offset
    }, 500);


}).on('click', '.calendar-month',function () {

    var that = $(this);
    var month = that.attr('data');
    var list = $('.calendar-month');

    list.removeClass('active');
    goToMonth(month);
    that.addClass('active');

}).on('click', '.event-text .text',function () {

    $('.map-wrap').fadeIn();
    mapG2.init();


}).on('click', '.map-type a',function () {

    var that = $(this);
    $('.map-type a').removeClass('active');
    var arrows = $('.partners-nav');
    that.addClass('active');
    if (that.hasClass('map-list')) {
        $('.blocks-six').hide();
        $('#partners-list').fadeIn();
        arrows.fadeIn();
    } else {
        $('.blocks-six').hide();
        $('#map').fadeIn();
        mapG.init();
        arrows.fadeOut();
    }


}).on('click', '.partner ',function () {

    var index = $(this).index();
    var descBlock = $('#partners-desc');
    var arrows = $('.partners-nav');
    arrows.fadeOut();
    $('.blocks-six').hide();

    descBlock.fadeIn().attr('partner', index);

}).on('click', '.contact-info-close',function () {

    var that = $(this);
    var arrows = $('.partners-nav');

    arrows.fadeIn();
    that.parent().hide();
    if ($('.map-type a.active').index() == 1) {
        $('#map').fadeIn();
        mapG2.init();
    } else {
        $('#partners-list').fadeIn();
    }


}).on('click', '.partners-nav a',function () {

    var that = $(this);
    if ($('#partners-list').is(':visible')) {
        var bar = $('.partner-list-bar');
        var oneBlock = Math.abs(parseInt($('.partner-col').width()));
        var currentPosition = Math.floor(Math.abs(parseInt($('.partner-list-bar').css('left')) / oneBlock));
        var offset = 0;
        var max = $('.partner-col').size() - 4;
        var menuItems = $('.point-nav a');
        var next = 0;

        if (that.hasClass('prev')) {
            if (currentPosition == 0) {
                offset = max * oneBlock + max;
                next = max;
            } else {
                next = currentPosition - 1;
                offset = next * oneBlock;
            }
        } else {
            if (currentPosition == max) {
                offset = 0;
                next = 0;
            } else {
                next = currentPosition + 1;
                offset = next * oneBlock;
            }
        }
        menuItems.removeClass('active');
        menuItems.eq(next).addClass('active');
        bar.stop().animate({
            left: -offset
        }, 500);
    } else if ($('#partners-desc').is(':visible')) {

        var index = that.attr('partner');


    } else {

    }


}).on('click', '.point-nav a',function () {

    var bar = $('.partner-list-bar');
    var index = $(this).index();
    var oneBlock = Math.abs(parseInt($('.partner-col').width()));
    var next = index * oneBlock;
    var menuItems = $('.point-nav a');

    menuItems.removeClass('active');
    menuItems.eq(index).addClass('active');
    bar.stop().animate({
        left: -next
    }, 500);
    return false;

}).on('mouseenter', '.info-icon',function () {

    var that = $(this);
    var tipBlock = $('.service-tip');
    var left = Math.floor(parseInt(that.offset().left + that.width())) + 10;
    var top = Math.floor(parseInt(that.offset().top - 215));

    tipBlock.css({
        left: left,
        top : top
    });

    tipBlock.stop().fadeIn();


}).on('mouseleave', '.info-icon',function () {

    var tipBlock = $('.service-tip');
    tipBlock.stop().fadeOut();


}).on('keypress', '.search-map',function () {


    $('.popups-wrapper').show();
    var that = $(this);
    var tipBlock = $('.search-tip');

    that.css({
        'color': 'black'

    });
    var left = Math.floor(parseInt(that.offset().left));
    var top = Math.floor(parseInt(that.offset().top + 30)) - $('.popups-wrapper').offset().top;

    tipBlock.css({
        left: left,
        top : top
    });

    tipBlock.stop().fadeIn();


}).on('click', '.search-tip > span',function () {

    var that = $(this);
    var searchBlock = $('.search-map');
    var tipBlock = $('.search-tip');

    searchBlock.css({
        'color': '#929292'

    });

    searchBlock.val(that.text());
    tipBlock.stop().fadeOut();

}).on('click', '.map-category',function () {

    $('.popups-wrapper').show();
    var that = $(this);
    var tipBlock = $('.category-tip');

    var left = Math.floor(parseInt(that.offset().left)) -50;
    var top = Math.floor(parseInt(that.offset().top + 50)) - $('.popups-wrapper').offset().top;

    tipBlock.css({
        left: left,
        top : top
    });

    tipBlock.stop().fadeIn();

}).on('click', '.category-tip > span',function () {

    $('.popups-wrapper').hide();
    var that = $(this);
    var categoryBlock = $('.map-category');
    var tipBlock = $('.category-tip');

    categoryBlock.text(that.text());
    tipBlock.stop().fadeOut();

}).on('click', '.popup-layout',function () {

    $('.popups-wrapper').hide();
    var tipBlock = $('.tips');
    var searchBlock = $('.search-map');

    searchBlock.css({
        'color': '#929292'

    });
    tipBlock.stop().fadeOut();

}).on('click', '.send-btn',function () {

    var form = $(this).parent();
    var errors = 0;

    if ($.trim(form.find('input').eq(0).val()) == '' || $.trim(form.find('input').eq(0).val()) == 'Заполните поле!') {
        form.find('input').eq(0).val('Заполните поле!').addClass('error');
        errors++;
    } else {
        form.find('input').eq(0).removeClass('error');
    }

    if ($.trim(form.find('input').eq(1).val()) == '' || $.trim(form.find('input').eq(1).val()) == 'Заполните поле!') {
        form.find('input').eq(1).val('Заполните поле!').addClass('error');
        errors++;
    } else {
        form.find('input').eq(1).removeClass('error');
    }

    if (errors <= 0) {
        $('.reg-form').fadeOut();
        $('.answer').fadeIn();
        form.find('input').val('');
    }
    return false;


}).on('mouseenter', '.event-text .text',function () {

    var that = $(this);

    that.addClass('hover');


}).on('mouseleave', '.event-text .text',function () {

    var that = $(this);

    that.removeClass('hover');


}).on('click', '.services-open',function () {
    $('.sevices').slideDown(500);
    $('html, body').animate({
        scrollTop: parseInt($(".sevices").offset().top) - 50
    }, 1000);

}).on('click', '.services-list-close',function () {
    $('.sevices').slideUp(1000);
    //$('html, body').animate({
    //    scrollTop: $(".club-life-block-four").offset().top - 50
    //}, 1000);

}).on('click', '.play-button',function () {
    var blockVideo = $('.video-center');
    var html = '<iframe width="850" height="510" src="//www.youtube-nocookie.com/embed/N59I6YXAh-0?rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe>';

    blockVideo.slideDown(500,function(){

        blockVideo.html(html);
    });
}).on('click' , '.close-video-button' , function(){
    var blockVideo = $('.video-center');
    blockVideo.fadeOut('fast' , function(){
        blockVideo.html('')
    });
    $('.club-life-block-three').slideUp(500,function(){

        //$('html, body').animate({
        //    scrollTop: $(".club-life-block-two").offset().top - 50
        //}, 500);
    });

}).on('click', '.watch-video', function () {
    $(".club-life-block-three").slideDown();
    $('html, body').animate({
        scrollTop: $(".club-life-block-three").offset().top - 50
    }, 1000);
    $('.play-button').trigger('click');

}).on('click', '.join-to-club', function(){
    $('html, body').animate({
        scrollTop: $(".club-life-block-eight").offset().top - 50
    }, 1000);


}).on('mouseenter', '.pdf-button, .about-programm',function(){
    $('.about-programm').css({
        'text-decoration' : 'none'
    });

}).on('mouseleave', '.pdf-button, .about-programm',function(){
    $('.about-programm').css({
        'text-decoration' : 'underline'
    });

});

function goToMonth(m) {

    var month = parseInt(m);
    var bar = $('.calendar-bar');
    if (bar.find('[data-m="' + month + '"]').eq(0).size()) {
        bar.find('[data-m="' + month + '"]').eq(0).find('.day').trigger("click")
    }

}

function showPartner() {

    var descBlock = $('#partners-desc');
    $('.blocks-six').hide();
    descBlock.fadeIn();

}

function partnersInit() {

    var size = $('.partner-col').size() - 2;
    var menu = $('.point-nav');
    var oneBlock = Math.abs(parseInt($('.partner').width()));
    var currentPosition = Math.floor(Math.abs(parseInt($('.partner-list-bar').css('left')) / oneBlock));
    menu.html('');

    for (var i = 1; i < size; i++) {
        menu.append('<a href="javascript: return false"></a>')
    }
    $('.point-nav a').eq(currentPosition).addClass('active');

    var widthMenu = $('.point-nav').width();
    var widthParent = $('#partners-list').width();
    var nextWidth = (widthParent / 2 ) - (widthMenu / 2);
    menu.css({
        'left': nextWidth

    })
}
$(window).resize(function () {
    partnersInit();

    if($('.category-tip.tips').is(':visible')) {
        $('.map-category ').trigger('click')
    }
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {


    } else {

        var rightPoint = $('.partner-col').size() * $('.partner-col').width() - ($('#partners-list').width() + $('#partners-list').offset().left);
        $('.partner-list-bar').draggable({ axis: 'x', containment: [-rightPoint, 0, $('#partners-list').offset().left, 0] });
        $('.calendar-bar').draggable({ axis: 'x',
            start : function(event, ui){
                $('.calendar-bar').css({
                    'cursor' : 'grabbing',
                    'cursor' : '-moz-grabbing',
                    'cursor' : '-webkit-grabbing'
                });
            },
            stop  : function (event, ui) {
                $('.calendar-bar').css({
                    'cursor' : 'pointer'
                });
                var events = $('.event');
                var offsetBar = Math.abs(ui.offset.left);
                var newCurrent = 0;
                $.each(events, function (i, elem) {
                    var offsetEvent = events.eq(i).offset().left;
                    var number = i;
                    if (offsetEvent > 0 && newCurrent == 0) {
                        newCurrent = 1;
                        events.removeClass('current')
                        events.eq(number).addClass('current');
                    }

                })


            } });
    }

});
$(document).ready(function () {
//    var elem = $('.scroll-services').jScrollPane({
//        hideFocus            : true,
//        verticalDragMinHeight: 0,
//        verticalDragMaxHeight: 54,
//        autoReinitialise     : true,
//        mouseWheelSpeed      : 50,
//        contentWidth         : '0px'
//    });
//
//    window.api = elem.data('jsp');

    partnersInit();

    if ($('.event.current').eq(0).size()) {
        $('.event.current').eq(0).find('.day').trigger('click');
    }
    var hammertime2 = new Hammer($('#partners-list').get(0));
    hammertime2.on('swipe drag', function (e) {
        switch (e.direction) {
            case 4:
                $('.partners-nav a.prev').trigger("click");
                break;
            case 2:
                $('.partners-nav a.next').trigger("click");
                break;
            case 'left':
                $('.partners-nav a.prev').trigger("click");
                break;
            case 'right':
                $('.partners-nav a.next').trigger("click");
                break;
            case 'up':
                return false;
                break;
            case 'down':
                return false;
                break;
        }
    });
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {

        var hammertime2 = new Hammer($('.calendar-wrap').get(0));
        hammertime2.on('swipe drag', function (e) {
            switch (e.direction) {
                case 4:
                    $('.nav-events a.prev').trigger("click");
                    break;
                case 2:
                    $('.nav-events a.next').trigger("click");
                    break;
                case 'left':
                    $('.nav-events a.prev').trigger("click");
                    break;
                case 'right':
                    $('.nav-events a.next').trigger("click");
                    break;
                case 'up':
                    return false;
                    break;
                case 'down':
                    return false;
                    break;
            }
        });


    } else {
//        var rightPoint = $('.partner-col').size() * $('.partner-col').width() - ($('#partners-list').width() + $('#partners-list').offset().left);
//        $('.partner-list-bar').draggable({ axis: 'x', containment: [-rightPoint, 0, $('#partners-list').offset().left, 0] });
        $('.calendar-bar').draggable({ axis: 'x',
            start : function(event, ui){
                $('.calendar-bar').css({
                    'cursor' : 'grabbing',
                    'cursor' : '-moz-grabbing',
                    'cursor' : '-webkit-grabbing'
                });
            },
            stop  : function (event, ui) {
                $('.calendar-bar').css({
                    'cursor' : 'pointer'
                });
                var events = $('.event');
                var offsetBar = Math.abs(ui.offset.left);
                var newCurrent = 0;
                $.each(events, function (i, elem) {
                    var offsetEvent = events.eq(i).offset().left;
                    var number = i;
                    if (offsetEvent > 0 && newCurrent == 0) {
                        newCurrent = 1;
                        events.removeClass('current')
                        events.eq(number).addClass('current');
                    }

                })


            } });
    }


})


function newActiveBlock () {
    var iter = 0;
    $.each($('.club-life-block'),function(i, elem) {
        var screenTop =  Math.floor($(document).scrollTop() - 50);
        var that = $('.club-life-block').eq(i);
        if(iter == 0) {
            var offset = Math.floor(that.offset().top);
            if( that.is(':visible') && offset > screenTop ) {
                iter++;
                $('.club-life-block').removeClass('active');
                $('.club-life-block').eq(i).addClass('active')
            }
        }

    })
}

newActiveBlock();
if(( $('.club-life-block.active').index()-2) > 0) {
    $('.b-main-nav').addClass('fixed-position');
}
if(( $('.club-life-block.active').index()-2) <= 0) {
    $('.club-life-block').removeClass('active');
}
var scroll = 0;
$(document).ready(function(){
    if($(window).scrollTop() > $('.header-fix-height').height()) {
        $('.b-main-nav').addClass('fixed-position');
    }
}).on('mousewheel',function(e,delta){
    //newActiveBlock();
    //var windowHeight = $( window ).height();
    //if(scroll == 0) {
    //    scroll = 1;
    //    if (delta < 0) {
    //        way = 'down';
    //    }
    //    if (delta > 0) {
    //        way = 'up';
    //    }
    //    if(!$('.club-life-block').hasClass('active')) {
    //        $('.club-life-block').eq(0).addClass('active');
    //    }
    //    if($('.club-life-block.active').index() != 0) {
    //        var active = $('.club-life-block.active').index() - 2;
    //
    //    } else {
    //        var active = 0;
    //    }
    //    if (active < 0 ) {
    //        active = 0;
    //    }
    //    var preActive = Math.floor($('.club-life-block.active').offset().top);
    //    var preActiveElem = active;
    //    $('.club-life-block').removeClass('active');
    //    if(way == 'up') {
    //
    //        active--;
    //        if($('.club-life-block').eq(active).is(':visible')) {
    //            $('.club-life-block').eq(active).addClass('active');
    //        }
    //        else {
    //            active--;
    //            $('.club-life-block').eq(active).addClass('active');
    //        }
    //        if($('.b-footer').hasClass('active')) {
    //            $('.b-footer').removeClass('active');
    //            active = $('.club-life-block').size();
    //        }
    //        if( active < 0 ) {
    //            active = 0;
    //            $('.b-main-nav').removeClass('fixed-position');
    //            $('.club-life-block').removeClass('active');
    //            $('html, body').animate({
    //                scrollTop: 0
    //            },{
    //                duration: 1000,
    //                complete : function(){scroll = 0;}
    //            });
    //        } else {
    //            if($('.club-life-block.active').size()) {
    //                var offs = Math.floor($('.club-life-block.active').offset().top) - 50;
    //            }
    //                else {
    //                var offs = 0;
    //            }
    //            if($(window).scrollTop() + windowHeight == preActive + $('.club-life-block').eq(preActiveElem).height()) {
    //                active = preActiveElem;
    //                var elem = $('.club-life-block').eq(active);
    //                offs = Math.floor(elem.offset().top);
    //                if($('.b-main-nav').hasClass('fixed-position')) {
    //                    offs = offs - 50;
    //                }
    //                $('.club-life-block').removeClass('active');
    //                elem.addClass('active');
    //            }
    //            $('html, body').animate({
    //                scrollTop: offs
    //            },{
    //                duration: 1000,
    //                complete : function(){scroll = 0;}
    //            });
    //        }
    //
    //    }else if (way == 'down') {
    //        if( $('.b-footer').hasClass('active')) {
    //                scroll = 0;
    //            } else {
    //            //$('.b-main-nav').addClass('fixed-position');
    //
    //                active++;
    //                if($('.club-life-block').eq(active).is(':visible')) {
    //                    $('.club-life-block').eq(active).addClass('active');
    //                }
    //                else {
    //                    active++;
    //                    $('.club-life-block').eq(active).addClass('active');
    //                }
    //
    //                if($(window).scrollTop() + windowHeight < preActive + $('.club-life-block').eq(preActiveElem).height()) {
    //                    active = preActiveElem;
    //                    var elem = $('.club-life-block').eq(active);
    //                    var offs = preActive + (elem.height() - windowHeight);
    //                    $('.club-life-block').removeClass('active');
    //                    elem.addClass('active');
    //
    //
    //                } else {
    //                    if($('.club-life-block.active').size()) {
    //                        var offs = Math.floor($('.club-life-block.active').offset().top) - 50;
    //                    } else {
    //                        var offs =  Math.floor($('.b-footer').offset().top);
    //                    }
    //                }
    //                if($(window).scrollTop() + 50 + windowHeight >  $('.b-footer').offset().top) {
    //                    active = $('.club-life-block').size();
    //                    $('.b-footer').addClass('active');
    //                    var offs = Math.floor($('.b-footer').offset().top) - 50;
    //                }
    //
    //
    //            $('html, body').animate({
    //                scrollTop: offs
    //            },{
    //                duration: 1000,
    //                complete : function(){scroll = 0;}
    //            });
    //
    //        }
    //    }
    //} else {
    //}
    //return false;
});

//hide popup

$(window)
    .on('resize', function(){
        $('.popups-wrapper').fadeOut(150);
    })
    .on('scroll', function(){
        $('.popups-wrapper').fadeOut(150);
    })
;
function initialize() {
    var zoomIn = document.getElementById('ofices-map-controls_zoom__in');
    var zoomOut = document.getElementById('ofices-map-controls_zoom__out');
    var zoomValue = 17;
    var mapCanvas = document.getElementById('ofices-map');
    var mapCanvas2 = document.getElementById('ofices-map-2');
    var myLatLng = new google.maps.LatLng(55.7548683,37.6344741);

    var styles =[
        {
            featureType: "all",
            elementType: "all",
            stylers: [
                { saturation: -100 } // <-- THIS
            ]
        }
    ];

    var mapOptions = {
        scrollwheel: false,
        navigationControl: false,
        mapTypeControl: false,
        scaleControl: false,
        center: myLatLng,
        zoom: zoomValue,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
        styles: styles
    };

    var map = new google.maps.Map(mapCanvas, mapOptions);
    var map2 = new google.maps.Map(mapCanvas2, mapOptions);

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: 'images/ofices-map.png',
        title:"Hello World!"
    });

    google.maps.event.addDomListener(zoomIn, 'click', function() {
        if (zoomValue >= 0 && zoomValue < 20) {
            zoomValue += 1;
            //console.log(zoomValue);

        }
        map.setZoom(zoomValue);
    });

    google.maps.event.addDomListener(zoomOut, 'click', function() {
        if (zoomValue > 0) {
            zoomValue -= 1;
            //console.log(zoomValue);
        }

        map.setZoom(zoomValue);
    });

    google.maps.event.addListener(marker, 'mouseover', function() {
        marker.setIcon('images/ofices-map-hover.png');
    })

    google.maps.event.addListener(marker, 'mouseout', function() {
        marker.setIcon('images/ofices-map.png');
    })

    var state; // 'show'

    function checkOffset () {
        if ($('.b-main-nav').hasClass('fixed-position')) {
            return -59;
        } else {
            return -137;
        }
    }

    $('.js-ofice-item').on('click', function() {





        var wrapList = $('.ofice-list');
        var popupToShow = $(this).parent().next();

        if (state == 'hide' || state == undefined) {
            state = 'show';
            $(this).parent().addClass('active-parent');


            $(this).addClass('ofice-item_active');
            //popupToShow.velocity({ height: 'auto'},  {duration: 400 });
            popupToShow.slideDown(400);
            popupToShow.velocity("scroll", { duration: 300, easing: "easeOut", offset: checkOffset() });
            wrapList.addClass('open');

        } else if (state == 'show' && !($(this).hasClass('ofice-item_active'))) {

            $('.js-ofice-item').removeClass('ofice-item_active');
            $(this).addClass('ofice-item_active');

            wrapList.addClass('open');
            $('.js-ofices-popup').slideUp(300);
            $('.js-ofices-popup').velocity('slideUp',  400, function () {
                //popupToShow.velocity({ height: 'auto'},  {duration: 500 });
                popupToShow.slideDown(500);
                popupToShow.velocity("scroll", { duration: 300, easing: "easeOut", offset: checkOffset() });
            });

        } else {

            wrapList.removeClass('open');
            $(this).parent().removeClass('active-parent');
            //$('.js-ofices-popup').velocity({ height: 0},  {duration: 600 });
            $('.js-ofices-popup').slideUp(400);
            $('.js-ofice-item').removeClass('ofice-item_active');

            state = 'hide';

        }



        var slickTimeOut = setTimeout(function(){
            popupToShow.find('.ofices-slider').slick({
                slidesToShow: 3,
                slidesToScroll: 3,
                dots: true,
                prevArrow: $('.ofice-slider_nav__left'),
                nextArrow: $('.ofice-slider_nav__right')
            });
            google.maps.event.trigger(map, 'resize');
            google.maps.event.trigger(map2, 'resize');
            clearTimeout(slickTimeOut);
        },800);
        slickTimeOut;
    });

    $('.ofices-map-controls_close').on('click', function() {

        var wrapList = $('.ofice-list');
        wrapList.removeClass('open');
        //$('.js-ofices-popup').velocity({ height: 0},  {duration: 400 });
        $('.js-ofices-popup').slideUp(400);
        state = 'hide';
        $('.js-ofice-item').removeClass('ofice-item_active');

    });



}

google.maps.event.addDomListener(window, 'load', initialize);

function initializeBig() {
    var zoomIn = document.getElementById('ofices-map-controls_zoom__in_big');
    var zoomOut = document.getElementById('ofices-map-controls_zoom__out_big');
    var zoomValue = 17;
    var mapCanvas = document.getElementById('ofices-map-big');
    var myLatLng = new google.maps.LatLng(55.7548683,37.6344741);

    var styles =[
        {
            featureType: "all",
            elementType: "all",
            stylers: [
                { saturation: -100 } // <-- THIS
            ]
        }
    ];

    var mapOptions = {
        center: myLatLng,
        zoom: zoomValue,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
        styles: styles
    }

    var map = new google.maps.Map(mapCanvas, mapOptions);

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: 'images/ofices-map.png',
        title:"Hello World!"
    });

    google.maps.event.addDomListener(zoomIn, 'click', function() {
        if (zoomValue >= 0 && zoomValue < 20) {
            zoomValue += 1;
            //console.log(zoomValue);

        }
        map.setZoom(zoomValue);
    });

    google.maps.event.addDomListener(zoomOut, 'click', function() {
        if (zoomValue > 0) {
            zoomValue -= 1;
            //console.log(zoomValue);
        }

        map.setZoom(zoomValue);
    });

    $('.btn_ofice-map').on('click', function () {

        $('.ofice-item').removeClass('ofice-item_active');
        $(this).addClass('btn_ofice-map__active');


        $('.btn_ofice-list').removeClass('btn_ofice-list__active');

        $('.js-ofices-popup').velocity({ height: 0},  0)

        $('.ofices-items').velocity("transition.slideDownOut", 200, function() {


            $('.ofices-map-big').velocity("transition.slideDownIn", 300, function() {
                $('.ofices-items_mapholder__big').show();
                google.maps.event.trigger(map, 'resize');
            });

        })

    })

}

google.maps.event.addDomListener(window, 'load', initializeBig);

$('.btn_ofice-list, .js-close-big').on('click', function () {
    $('.btn_ofice-list').addClass('btn_ofice-list__active');
    $('.ofices-items_mapholder__big').hide();
    $('.btn_ofice-map').removeClass('btn_ofice-map__active');

    $('.ofices-map-big').velocity("transition.slideUpOut", 200, function() {
        $('.ofices-items').velocity("transition.slideUpIn", { stagger: 200 })
    });

});


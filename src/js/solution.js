$(document).ready(function(){

    // Добавление скидки
    $('.b-added-discount_btn-add').on('click', function(){
        $('.b-added-discount_btn-add').fadeOut(function() {
            $('.b-added-discount_done').fadeIn();
        });
    });
    $('.b-added-discount_remove').on('click', function(){
        $('.b-added-discount_done').fadeOut(function(){
            $('.b-added-discount_btn-add').fadeIn();
        });
    });


    // Попап под почту
    $('.mail-link').on('click', function(){
        $('.b-overlay').fadeIn();
        $('body').addClass('o-h');
        $('.b-overlay_popup label input[type="text"]').val('');
    });
    $('.b-overlay_popup_close').on('click', function(){
        $('.b-overlay').fadeOut();
        $('body').removeClass('o-h');
    });
    $('.b-overlay').click(function(event) {
        if( $(event.target).closest(".b-overlay_popup").length){
            return;
        }
        $(".b-overlay").fadeOut();
        $('body').removeClass('o-h');
        event.stopPropagation();
    });
    $('.b-overlay_popup_submit').on('click', function(){
        $('.b-overlay_popup_data').fadeOut(function(){
            $('.b-overlay_popup_thanks').fadeIn();
        });
    });

});

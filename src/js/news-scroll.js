/*
 * jQuery throttle / debounce - v1.1 - 3/7/2010
 * http://benalman.com/projects/jquery-throttle-debounce-plugin/
 *
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function(b,c){var $=b.jQuery||b.Cowboy||(b.Cowboy={}),a;$.throttle=a=function(e,f,j,i){var h,d=0;if(typeof f!=="boolean"){i=j;j=f;f=c}function g(){var o=this,m=+new Date()-d,n=arguments;function l(){d=+new Date();j.apply(o,n)}function k(){h=c}if(i&&!h){l()}h&&clearTimeout(h);if(i===c&&m>e){l()}else{if(f!==true){h=setTimeout(i?k:l,i===c?e-m:e)}}}if($.guid){g.guid=j.guid=j.guid||$.guid++}return g};$.debounce=function(d,e,f){return f===c?a(d,e,false):a(d,f,e!==false)}})(this);


var windowWidth = $('.news').width();
var leftColHeight = $('.news-left').height();
var columnWidth = $('.b-column').width();
var rightColWidth = $('.news-right').width();

function getRightWidth (windowWidth, leftColHeight, columnWidth, rightColWidth) {
    //console.log(windowWidth)
    if (windowWidth < 1000) {
        $('.news_wrap').width(355);
        $('.news_wrap').slimScroll({
            width: 355,
            height: leftColHeight + 100,
            railVisible: true,
            alwaysVisible: false,
            size: '16px'
        });
    } else {
        var paddingLeft = (windowWidth - columnWidth) / 2
        var diffWidth = paddingLeft + rightColWidth;
        $('.news-right, .news_wrap').height(leftColHeight + 100);
        $('.news-right-item').css('padding-right', paddingLeft);
        $('.news_wrap').width(diffWidth);
        $('.news_wrap').slimScroll({
            width: diffWidth,
            height: leftColHeight + 100,
            railVisible: true,
            alwaysVisible: false,
            size: '16px'
        });
    }
}

getRightWidth (windowWidth, leftColHeight, columnWidth, rightColWidth);

$( window ).resize(function() {
    if ($(this).width() > 1000) {
        windowWidth = $('.news').width();
        leftColHeight = $('.news-left').height();
        columnWidth = $('.b-column').width();
        rightColWidth = $('.news-right').width();
        $('.news_wrap').slimScroll({destroy: true});
        getRightWidth (windowWidth, leftColHeight, columnWidth, rightColWidth);
    }
});


;(function ($) {
    var on = $.fn.on, timer;
    $.fn.on = function () {
        var args = Array.apply(null, arguments);
        var last = args[args.length - 1];

        if (isNaN(last) || (last === 1 && args.pop())) return on.apply(this, args);

        var delay = args.pop();
        var fn = args.pop();

        args.push(function () {
            var self = this, params = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                fn.apply(self, params);
            }, delay);
        });

        return on.apply(this, args);
    };
}(this.jQuery || this.Zepto));


$('.news_wrap').scroll($.debounce( 300, true, function(){
    $('.news_wrap').addClass('news_wrap_scroll')
}));
$('.news_wrap').scroll($.debounce( 300, function(){
    $('.news_wrap').removeClass('news_wrap_scroll')
}));
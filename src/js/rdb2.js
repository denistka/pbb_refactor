(function() {
    var serviceLink = $('.rdb-services_left a');
    var parentOffset = $('.rdb-services_left').offset().top + 18;
    var firstLinkOffset = $('.rdb-services_left a.active').offset().top - parentOffset;

    function checkOffset () {
        if ($('.b-main-nav').hasClass('fixed-position')) {
            return -59;
        } else {
            return -137;
        }
    }

    $('.btn-more').on('click', function() {
        $('.rdb-services').velocity("scroll", { duration: 300, easing: "easeOut", offset: checkOffset() });
    })

    serviceLink.on('click', function(e){
        //console.log($(this).offset().top, parentOffset);
        var linkOffset = $(this).offset().top - parentOffset;
        var currentNavItem = $(this).attr('id').slice(8);
        var currentProp = $('#js-personal-' + currentNavItem);
        var currentPropItems = $('.rdb-services-list_item', currentProp);
        var openedProp = $('.rdb-services-personal-items_visible');
        var openedPropItems = $('.rdb-services-personal-items_visible .rdb-services-list_item');

        var mySequence = [
            {elements: openedPropItems, properties: "transition.flipYOut", options: {stagger: 50, duration: 50, complete: function() {openedProp.removeClass('rdb-services-personal-items_visible')}} },
            {elements: currentPropItems, properties: "transition.flipYIn", options: {stagger: 80, duration: 200, begin: function() {currentProp.addClass('rdb-services-personal-items_visible')}} }
        ];




        serviceLink.removeClass('active');
        $(this).addClass('active');

        if (!(currentProp.hasClass('rdb-services-personal-items_visible'))) {
            $.Velocity.RunSequence(mySequence);
        }



        $('.indicator').velocity({
            'top': linkOffset - 9 + "px"
        },180, [.29,.03,.22,1]);



        e.preventDefault();


    })
})();
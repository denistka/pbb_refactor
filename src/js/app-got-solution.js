$(document).on('scroll',function (e) {

    var newScroll = $(window).scrollTop();
    var offset = $('.solution-content-one').offset().top - $('.global-header').height();
    var clone = $('.red-blocks').clone();
    var that = $('.red-fixed');

    if (newScroll > offset) {
        if (!that.hasClass('open')) {
            that.find('.solution-content-right').empty();
            that.addClass('open');
            that.stop().slideDown();
            that.find('.solution-content-right').html(clone);
        } else {
            return;
        }
    } else {
        that.find('.solution-content-right').empty();
        that.removeClass('open');
        that.stop().slideUp();
    }

}).on('mouseenter', '.solution-content-four .circle-right', function () {

    var block = $(this);

    var search = block.find('.red-tip-under');

    if(search.size()) {
        search.stop().animate({
            'opacity' : 1
        } , 1000);
    }

}).on('mouseleave', '.solution-content-four .circle-right', function () {

    var block = $(this);

    var search = block.find('.red-tip-under');

    if(search.size()) {
        search.stop().animate({
            'opacity' : 0
        } , 1000);
    }

}).on('mouseenter', '.solution-content-blocks-one', function () {

    var block = $(this);

    var search = block.find('.red-tip');
    if(search.size()) {
        search.stop().animate({
            'opacity' : 1
        } , 1000);
    }

}).on('mouseleave', '.solution-content-blocks-one', function () {

    var block = $(this);

    var search = block.find('.red-tip');

    if(search.size()) {
        search.stop().animate({
            'opacity' : 0
        } , 1000);
    }

}).on('mouseenter', '.solution-content-blocks-three .gold', function () {

    var block = $(this).parent();

    var search = block.find('.gold-tip-under');
    if(search.size()) {
        search.stop().animate({
            'opacity' : 1
        } , 1000);
    }

}).on('mouseleave', '.solution-content-blocks-three .gold', function () {

    var block = $(this).parent();

    var search = block.find('.gold-tip-under');

    if(search.size()) {
        search.stop().animate({
            'opacity' : 0
        } , 1000);
    }

}).on('mousedown' , '.checkbox-solutions ' , function(){

    var that = $(this);
    if(!that.hasClass('disable')) {
        if(that.hasClass('active')) {
            that.parent().removeClass('available_checkbox');
            that.removeClass('active');
        } else {
            that.addClass('active');
            that.parent().addClass('available_checkbox');
        }

    }
    return false;
}).on('mousedown', '.checkboxLink' , function(){

    console.log($(this).parent().find('.checkbox-solutions'));
    $(this).parent().find('.checkbox-solutions').trigger('mousedown')



}).on('mouseenter', '.solution-content-blocks-two', function () {

    var block = $(this);
    var search = block.find('.tips');

    if(search.size()) {
        search.stop().animate({
            'opacity' : 1
        } , 1000);
    }

}).on('mouseleave', '.solution-content-blocks-two ', function () {

    var block = $(this);

    var search = block.find('.tips');

    if(search.size()) {
        search.stop().animate({
            'opacity' : 0
        } , 1000);
    }

});
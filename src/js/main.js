$(window).load(function () {

    $("body").addClass("offices-active");
});
$(window).ready(function () {
    rko.table();
    $('.b-index-menu_item').hover(function () {
        var $self = $(this);
        $self.parent().removeClass('products solutions services').addClass($self.data('name'));
    }, function () {
        var $self = $(this);
        $self.parent().removeClass('products solutions services');
    });

    $('.b-widget-contacts_caption_btn').click(function () {
        //$('.b-widget-contacts_caption_cnt').slideToggle();
        $('.b-widget-contacts_caption_row').slideToggle();
        //$('.b-widget-contacts_caption_title, .b-widget-contacts_caption_row').slideToggle();
        $('.b-widget-contacts_caption_name').toggleClass('_active');
        $(".b-widget-contacts_caption_btn").toggleClass("_down");
    });


    // Функция открытия окна выбора города
    $('.b-select-city_current').on('click', function (event) {
        var $self = $(this),
            $parent = $self.parent();
        $parent.toggleClass('_offices');
        event.stopPropagation();
    });

    $(document).on("click", function (event) {
        if ($(event.target).closest(".b-select-offices").length ||
            $(event.target).closest(".b-select-city_cnt").length ) {
            return;
        }
        if ($(".b-select-city").hasClass("_offices")) {
            $(".b-select-city").removeClass("_offices");
        }

        if ($(".b-select-city").hasClass("_city")) {
            $(".b-select-city").removeClass("_city");
        }
        event.stopPropagation();
    })


    $(document).on('click', '.b-select-offices .close, .b-select-city_cnt .close', function (event) {
        $('.b-select-city').removeClass('_offices');
        $(".b-select-city").removeClass("_city");

        event.stopPropagation();
    });


    $(document).on('click', ".b-top-line_search_btn", function () {
        $('body').toggleClass('search-active');
        if ($('body').hasClass('search-active')) {
            $('.b-top-line_search-text').focus();
        }
    });

//    mail

    $(document).on('click', '.js_call', function(event) {
        $('span', this).hide();
        $('.l-summary-row__book-call',this).show();


        $('.l-summary-row__book-call input',this).focus();
        $('.call-button',this).show();

        event.stopPropagation();

    })

    $('.js_telephone').blur(function() {
        if ($(this).val() < 19) {
            //console.log($(this).val().length)

            $('.l-summary-row__book-call').hide();
            $('.js_call span').show();
        }

    })

    $(document).on('click', ".b-top-line_mail_btn", function (event) {
        $('body').toggleClass('mail-active');
        event.stopPropagation();

        return false;
    });

    $(document).on('click', '.b-top-line_mail_popup-window .close, .b-top-line_mail_popup-overlay', function (event) {
        $('body').removeClass('mail-active');
        event.stopPropagation();
    });

//    select city


    $(document).on('click', '._offices .b-select-offices__name a', function (event) {
        var that = $(this);
        var block = that.parent().parent().parent().parent().find(".b-select-city");
        block.removeClass("_offices").addClass("_city");
        event.stopPropagation();
    });

    $(".b-select-city_item", ".b-select-city").on('click', function (event) {
        var that = $(this);
        that.parent().parent().parent().parent().parent().find(".b-select-city").addClass("_offices").removeClass("_city");
        var $city = $(this).text();
//    that.parent().parent().parent().parent().parent().find("a", ".b-select-offices__name").text($city);
        that.parent().parent().parent().parent().parent().find(".b-select-offices__name a").text($city);
        event.stopPropagation();
    });

//    office popup

    $(document).on('click', '.b-top-line_offices_popup .close, .b-top-line_offices_popup-overlay, .b-select-offices__item', function (event) {
        $('body').removeClass('offices-active');
        event.stopPropagation();
    });

    $(document).on('click', '.b-select-offices__item', function (event) {
        var $office = $(this).find(".b-select-offices__title").text(),
            $address = $(this).find(".b-select-offices__address").text();

        $(".b-widget-contacts_caption_name a").text($office);
        $(".b-widget-contacts__address").text($address);

        $(".b-select-city").removeClass("_offices");
        event.stopPropagation();
    });

    $(".b-select-offices__name a", ".b-top-line_offices_popup").on("click", function (event) {
        $("body").removeClass("offices-active").addClass("city-active");
        event.stopPropagation();
    });

//    city popup

    $(document).on('click', '.b-top-line_city_popup .close, .b-top-line_city_popup-overlay', function (event) {
        $('body').removeClass('city-active');
        event.stopPropagation();
    });

    $(".b-select-city_item", ".b-top-line_city_popup").on("click", function (event) {
        $("body").removeClass("city-active").addClass("offices-active");
        var $city = $(this).text();
        $("a", ".b-select-offices__name").text($city);
        event.stopPropagation();
    });



// интернет банк

    $(".b-top-line_e-life").on("click", function (event) {
        $(".b-top-line_e-life_btns").slideToggle();
        event.stopPropagation();
        return false;
    });

    $(document).on("click", function (event) {
        if ($(event.target).closest(".b-top-line_e-life").length)
            return;
        $(".b-top-line_e-life_btns").slideUp();
        event.stopPropagation();
    });


 // customselect

    if($('.select-wrapper select').size() > 0)
        $('.select-wrapper select').customSelect();


 // main blocks animations
    $(document).on('mouseleave' ,'.main-service', function() {
        var that = $(this);
        var title =  $('.service-scheme-title', that);
        var content =  $('.main-service-content', that);
        //console.log('enter in this loop',content);
        if(content.hasClass('main-service-content_show')) {
            //console.log('enter in this loop 2');
            $('.service-figure', that).removeClass('figure-active');


            if (that.hasClass('main-service_blue')) {
                $('.arrow-left').removeClass('arrow-main_hide').addClass('arrow-main_show');
                $('.ico-main_blue').removeClass('ico-main_show').addClass('ico-main_hide');

            } else if (that.hasClass('main-service_pink')) {
                $('.arrow-left, .arrow-right').removeClass('arrow-main_hide').addClass('arrow-main_show');
                $('.ico-main_pink').removeClass('ico-main_show').addClass('ico-main_hide');

            } else {
                $('.arrow-right').removeClass('arrow-main_hide').addClass('arrow-main_show');
                $('.ico-main_green').removeClass('ico-main_show').addClass('ico-main_hide');

            }

            title.show();

            title.animo({animation: ['zoomIn'], duration: 0.15}, function () {
                title.show();
            });

            content.removeClass('main-service-content_show')
        }

    }).on('mouseenter' , '.service-scheme-title , .service-figure' , function(){
        var that = $(this).parent();
        var title =  $('.service-scheme-title', that);
        var content =  $('.main-service-content', that);

        if (that.hasClass('main-service_blue')) {
            $('.arrow-left').removeClass('arrow-main_show').addClass('arrow-main_hide');
            $('.ico-main_blue').removeClass('ico-main_hide').addClass('ico-main_show');
        } else if (that.hasClass('main-service_pink')) {
            $('.arrow-left, .arrow-right').removeClass('arrow-main_show').addClass('arrow-main_hide');
            $('.ico-main_pink').removeClass('ico-main_hide').addClass('ico-main_show');

        } else {
            $('.arrow-right').removeClass('arrow-main_show').addClass('arrow-main_hide');
            $('.ico-main_green').removeClass('ico-main_hide').addClass('ico-main_show');

        }

        $('.service-figure', that).addClass('figure-active');

        title.animo({animation: ['zoomOut'], duration: 0.15}, function () {
            title.hide();
        });

        content.addClass('main-service-content_show')
    });

    $(document).on('click' , '.b-select-city_lists .b-select-city_item' , function(){
        var that = $(this);
        var text = that.text();
        var block = $('.ofices-location > span');
        block.toggleClass('ofices-location_active').text(text);
        $('.b-select-city_ofices').velocity('transition.slideDownOut', 200);
    });

    $('.ofices-location span').on('click', function() {
        $(this).toggleClass('ofices-location_active');
        if ($(this).hasClass('ofices-location_active')) {
            $('.b-select-city_ofices').velocity('transition.slideUpIn', 200)

        } else {
            $('.b-select-city_ofices').velocity('transition.slideDownOut', 200)

        }

    });
    //$('.main-service').hover(
    //    function() {
    //
    //     var title =  $('.service-scheme-title', this);
    //     var content =  $('.main-service-content', this);
    //
    //     if ($(this).hasClass('main-service_blue')) {
    //            $('.arrow-left').removeClass('arrow-main_show').addClass('arrow-main_hide');
    //            $('.ico-main_blue').removeClass('ico-main_hide').addClass('ico-main_show');
    //     } else if ($(this).hasClass('main-service_pink')) {
    //             $('.arrow-left, .arrow-right').removeClass('arrow-main_show').addClass('arrow-main_hide');
    //         $('.ico-main_pink').removeClass('ico-main_hide').addClass('ico-main_show');
    //
    //     } else {
    //             $('.arrow-right').removeClass('arrow-main_show').addClass('arrow-main_hide');
    //         $('.ico-main_green').removeClass('ico-main_hide').addClass('ico-main_show');
    //
    //     }
    //
    //     $('.service-figure', this).addClass('figure-active');
    //
    //     title.animo({animation: ['zoomOut'], duration: 0.15}, function () {
    //            title.hide();
    //     });
    //
    //     content.addClass('main-service-content_show')
    //
    //    },
    //    function () {
    //        var title =  $('.service-scheme-title', this);
    //        var content =  $('.main-service-content', this);
    //        $('.service-figure', this).removeClass('figure-active');
    //
    //
    //        if ($(this).hasClass('main-service_blue')) {
    //            $('.arrow-left').removeClass('arrow-main_hide').addClass('arrow-main_show');
    //            $('.ico-main_blue').removeClass('ico-main_show').addClass('ico-main_hide');
    //
    //        } else if ($(this).hasClass('main-service_pink')) {
    //            $('.arrow-left, .arrow-right').removeClass('arrow-main_hide').addClass('arrow-main_show');
    //            $('.ico-main_pink').removeClass('ico-main_show').addClass('ico-main_hide');
    //
    //        } else {
    //            $('.arrow-right').removeClass('arrow-main_hide').addClass('arrow-main_show');
    //            $('.ico-main_green').removeClass('ico-main_show').addClass('ico-main_hide');
    //
    //        }
    //
    //        title.show();
    //
    //        title.animo({animation: ['zoomIn'], duration: 0.15}, function () {
    //            title.show();
    //        });
    //
    //        content.removeClass('main-service-content_show')
    //    }
    //)

    // ofices slide down animation

        // ofices slide down animation

    $('.ofice-map').on('click', function() {
        $(this).toggleClass('ofice-map_open')
        $('.main-offices-top').toggleClass('main-offices-top_open')
        return false;
    });




    if($('.main-big-promo').size() > 0) {
        //$('.global-content').css('height' , '100%');
        initPromo();
    }
    if($('.bg').size() > 0)

        slider();


});

function initPromo() {
    if($('.main-big-promo').size() > 0) {
        var windowHeight = $( window ).height();
        var headerHeight = $('.global-header').height();
        var customBottomOffset = 100;
        var minHeight = 483;
        var height = ((windowHeight - headerHeight - customBottomOffset) < minHeight) ? minHeight : (windowHeight - headerHeight - customBottomOffset);
        var elem = $('.main-big-promo .main-box-top_wrap');
        var padding = height - minHeight;

        //var arrows = $('.arrow-main');
        //arrows.removeAttr('style');
        //var arrowsTop = parseInt(arrows.css('top'));
        //arrows.css({
        //    'top' : arrowsTop + padding/2
        //});


        var paddingBlocks = $('.main-service > div');
        $.each(paddingBlocks, function(i , val){
            var that = $(val);
            if(that.hasClass('main-service-content')) {
                return;
            }
            that.removeAttr('style');
            var newTop = parseInt(that.css('top'));
            that.css({
                'top' : newTop + padding / 2
            });

        });



        elem.css({
            'height' : height
        });
    }

}
$(window).resize(function(){
    //console.log('window resize');


    $(function () {
        setTimeout(function () {
            rko.table();
        }, 300);

    });

    initPromo()
});
// Listen for orientation changes
window.addEventListener("orientationchange", function() {
    initPromo();
}, false);
// Listen for resize changes

function slider() {
    var i = 1;
    var slider;
    var slides = 2;
    var time = 10000;

    var func = function() {
        i++;
        if ($('.bg.active').length > 0) {
            slider = $('.bg.active');
        } else {
            slider = $('.bg');
        }
        if (i > slides) i = 1;
        var newBlock = slider.clone().attr('class', 'bg active bg_' + i);
        slider.before(newBlock);
        slider.delay(1000).fadeOut(1000, function () {
            slider.remove();
        });

        setTimeout(function () {
            func();
        }, time);
    };
    func();
}

var rko = {
    defaults : {
        parent : null,
        left : null,
        right : null,
        leftItems: null,
        rightItems: null,
        wrapSelector: '.table-wrap',
        itemSelector: '.trow-height',
        itemHeadSelector : 'thead',
        whiteBgSelector : '.dark-bg',
        arraySizes: null
    },
    init: function(){
        rko.defaults.parent = $(rko.defaults.wrapSelector);
        rko.defaults.left = rko.defaults.parent.find('.left');
        rko.defaults.right = rko.defaults.parent.find('.right');
        rko.defaults.leftItems = rko.defaults.left.find(rko.defaults.itemSelector);
        rko.defaults.rightItems = rko.defaults.right.find(rko.defaults.itemSelector);
    },
    table : function(){

        rko.init();
        rko.calc();
        rko.resize();

    },
    calc: function(){

        var count = rko.defaults.leftItems.length;
        var height = 0;
        var iter = 0;

        rko.defaults.arraySizes = new Array();

        rko.defaults.leftItems.each(function(i, item){

            var that = $(item);
            that.attr('style' , '');
            height = parseInt( that.css('height') );
            rko.defaults.arraySizes.push(height);

        });

        rko.defaults.rightItems.each(function(i,item){

            var that = $(item);

            that.attr('style' , '');
            if(!that.hasClass(rko.defaults.itemHeadSelector)){

                height = parseInt( that.css('height') );

                rko.defaults.arraySizes[iter] = (height > rko.defaults.arraySizes[iter]) ? height : rko.defaults.arraySizes[iter];


                iter++;
                if(iter >= count) iter = 0;

            }

        });

    },
    resize: function(){

        var iter = 0;
        var count = rko.defaults.leftItems.length;
        var totalH = 0;

        rko.defaults.leftItems.each(function(i,item){

            var that = $(this);
            that.css({
                'height' : rko.defaults.arraySizes[iter]
            });
            totalH += parseInt(that.outerHeight());
            iter++;
            if(iter >= count) iter = 0;


        });
        rko.defaults.rightItems.each(function(i,item){

            var that = $(this);
            if(!that.hasClass(rko.defaults.itemHeadSelector)){

                that.css({
                    'height' : rko.defaults.arraySizes[iter]
                });
                iter++;
                if(iter >= count) iter = 0;

            }

        });
        rko.defaults.parent.find(rko.defaults.whiteBgSelector).css({
            'height' : totalH + 50
        })
    }
};

//    select city

$(document).on('click', '._offices .b-select-offices__name a', function (event) {
    var that = $(this);
    var block = that.parent().parent().parent().parent().find(".b-select-city");
    block.removeClass("_offices").addClass("_city");
    event.stopPropagation();
});

$(".b-select-city_item", ".b-select-city").on('click', function (event) {
    var that = $(this);
    that.parent().parent().parent().parent().parent().find(".b-select-city").addClass("_offices").removeClass("_city");
    var $city = $(this).text();
//    that.parent().parent().parent().parent().parent().find("a", ".b-select-offices__name").text($city);
    that.parent().parent().parent().parent().parent().find(".b-select-offices__name").text($city);
    event.stopPropagation();
});

$(document).on('click', '.b-select-offices .close',function (event) {
    var that = $(this);
    that.parent().parent().parent().parent().find('.b-select-city').removeClass('_offices');

    event.stopPropagation();
});

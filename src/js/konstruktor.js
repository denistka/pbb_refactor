$('.c-trackbar_point').on('click', function( event ) {
    var numValue = $(this).data('value');
    var leftOffset = $(this).css('left');
    var par = $(this).parent().parent();
    var sPar = $(this).parent().parent().parent().parent();

    $('.c-trackbar_drag', par).css('left', leftOffset);
    $('.c-trackbar_track_fill', par).css('width', leftOffset);
    $('.c-trackbar_track_fill', par).css('width', leftOffset);
    if ($(this).hasClass('c-trackbar-date')) {
        $('input.slide-hidden', sPar).val(numValue).change();
    } else {
        $('input', sPar).val(numValue);

    }

});

$('.slide-hidden').on('change', function() {
    var currentMonths = $(this).val();
    var getYear = function() {
        if (currentMonths < 12) {
            return 0
        } else {
            return parseInt(currentMonths / 12);
        }
    };

    var getMonth = function() {
        if (currentMonths < 12) {
            return currentMonths;
        } else {
            return (currentMonths - (Math.floor((currentMonths / 12)) * 12))
        }
    };

    $('.js-month').val(getMonth);
    $('.js-year').val(getYear());


});

$(document).on('click', '.radioTitle' , function(){

    var that = $(this);
    var parent = that.parent();
    parent.find('input').prop( "checked", true );

});

$(document).on('click', '.kostruktor-chech__title' , function(){

    var that = $(this);
    var parent = that.parent();
    var bool = true;
    if(parent.find('input').prop( "checked")) {
      bool = false;
    }
    parent.find('input').prop( "checked", bool);

});


$('.js-year').on('input', function() {
    var newValue = parseInt(($(this).val() * 12)) + parseInt($('.js-month').val());
    $('.js_input_intervals_1').val(newValue).trigger('input');
}).change()


$('.js-month').on('input', function() {

    var newValue = parseInt(($('.js-year').val() * 12)) + parseInt($(this).val());
    $('.js_input_intervals_1').val(newValue).trigger('input');

}).change();



'use strikt';

(function () {
	App = window.App || {};

	App.ClubCalendar = function (params) {
		var self = this;

		this.options = {
			selector_head: 				'.b-club-calendar',
			selector_bar: 				'.b-club-calendar_bar',
			selector_headMonth: 		'.b-club-calendar_dates_month',
			selector_activeHeadMonth: 	'.b-club-calendar_dates_month__active',
			selector_months: 			'.b-club-calendar_bar_months',
			selector_monthsWraper: 		'.b-club-calendar_bar_months_w',
			selector_month: 			'.b-club-calendar_bar_month',
			selector_day: 				'.b-club-calendar_day',
			selector_dataDay:			'.b-club-calendar_day__data',
			selector_lastDay: 			'.b-club-calendar_day__last',
			selector_dayName: 			'.b-club-calendar_day_name',
			selector_dayNumber: 		'.b-club-calendar_day_number',
			selector_dayData: 			'.b-club-calendar_day_data',
			selector_dayDataArrow: 		'.b-club-calendar_day_data_arrow',
			selector_dayEventName: 		'.b-club-calendar_day_event_name',
			selector_activeDay: 		'.b-club-calendar_day__active',
			selector_navLeft: 			'.b-club-calendar_bar_nav-left',
			selector_navRight: 			'.b-club-calendar_bar_nav-right'
		};

		this.$head = $(this.options.selector_head);
		this.$el = $(this.options.selector_bar);
		this.$headMonthItems = this.$head.find(this.options.selector_headMonth);
		this.$days = this.$el.find(this.options.selector_day);
		this.$months = this.$el.find(this.options.selector_months);
		this.$monthsWrap = this.$el.find(this.options.selector_monthsWraper);
		this.$monthsItems = this.$el.find(this.options.selector_month);
		this.$navLeft = this.$el.find(this.options.selector_navLeft);
		this.$navRight = this.$el.find(this.options.selector_navRight);
		this.$carusel = this.$el.find('#owl-example');

		this.carusel = new Swiper (this.$carusel, {
			slidesPerView: 1,
			preventClicks: false,
			preventClicksPropagation: false,
			onSlideChangeEnd: function (e) { self.onSlide.call(this, e, self)},
			onInit: function (e) { self.onSlide.call(this, e, self)},
			onClick: function () {
			}
		});

		this.$el.on('click', this.options.selector_dayNumber, this.onClickDay.bind(this));
		this.$head.on('click', this.options.selector_headMonth, this.onClickMonth.bind(this));
		this.$navLeft.on('click', function () {
			self.$carusel.trigger('owl.prev')
		});
		this.$navRight.on('click', function () {
			self.$carusel.trigger('owl.next')
		});
		this.$el.on('click', this.options.selector_dayEventName, function (e) {
			e.preventDefault();
			e.stopPropagation();

			$('.map-wrap').fadeIn();
			mapG2.init();
		});
	};

	App.ClubCalendar.prototype = {
		onSlide: function(e, _class) {
			//debugger
			// Удаляем класс активного месяца у всех месяцев и добавляем класс к нужному месяцу
			_class.$headMonthItems.removeClass(_class.options.selector_activeHeadMonth.substr(1)).eq(e.activeIndex).addClass(_class.options.selector_activeHeadMonth.substr(1));
			_class.showActiveDay.call(_class, e.activeIndex);
			_class.selectNextDay.call(_class);
		},
		/**
		 * Показывает события дня, активного месяц
		 * @param month - Порядковый номер месяца
		 * @param day - номер дня начиная с 0
		 */
		showActiveDay: function(month) {
			var $month = this.$monthsItems.eq(month);
			var $day = $month.find(this.options.selector_day + this.options.selector_dataDay).not(this.options.selector_lastDay).first();
			this.showDay(month, $day);
		},
		showDay: function (month, $day) {
			if($day && $day.length && (month || month === 0)) {
				var offset = 0;
				var widthItem = $(window).width() < 1280 ? 36 - 8 : $day.prev().width();
				var position = $day.index() * widthItem + 20; // по умолчанию позиция слева
				var positionArrow = 54 / 2 - 4;

				this.$days.removeClass(this.options.selector_activeDay.substr(1));
				$day.addClass(this.options.selector_activeDay.substr(1));
				this.$days.css('margin', '');
				this.$days.css('padding', '');
				if($day.index() === $day.parent().find(this.options.selector_day).length - 1) {
					$day.css('margin', '0 0 0 20px');
				} else if($day.index() > 0) {
					$day.css('margin', '0 20px');
				} else {
					$day.css('margin', '0 20px 0 0');
				}
				if($(window).width() < 1280) {
					if($day.index() > 0) {
						$day.css('padding', '0 8px 0 0');
					}
				}

				if($day.index() && $day.index() > 0) {
					// Если блок выходит за край родителя, то смещаем его влево
					// на столько на сколько он выходит
					if((position + $day.find(this.options.selector_dayData).outerWidth(true)) > $day.parent().width())  {
						offset = $day.parent().width() - (position + $day.find(this.options.selector_dayData).outerWidth(true));
						position += offset;
						positionArrow -= offset;
					}
					$day.find(this.options.selector_dayData).css({
						'left': position,
						right: ''
					});
					$day.find(this.options.selector_dayDataArrow).css({
						'left': positionArrow
					});
				}
				this.changeMonthHeight(month, $day);
			}
		},
		onClickMonth: function (e) {
			var $self = $(e.currentTarget);
			var month = $self.data('month');
			this.carusel.slideTo(month);
		},
		onClickDay: function (e) {
			if (this.carusel.animating) {
				return false;
			}
			//console.log('Выбрали день');
			var $self = $(e.currentTarget).parents(this.options.selector_day);
			if($self.hasClass(this.options.selector_dataDay.substr(1))) {
				this.showDay(this.carusel.activeIndex, $self);
			};
		},
		/**
		 * показываем первый день текущего месяца
		 */
		selectNextDay: function (current) {
			this.$days.parent().find(this.options.selector_day + ' ' + this.options.selector_dataDay);
		},
		/**
		 * Пересчитываем высоту блока месяца в зависимости от высоты контента в выбранном дне
		 */
		changeMonthHeight: function (monthNumber,$day) {
			//console.log('Пересчитываем высоту блока месяца в зависимости от высоты контента в выбранном дне');
			//debugger;
			this.$monthsItems.height($day.outerHeight(true) + $day.find(this.options.selector_dayData).outerHeight(true) + 20);
			//this.$monthsItems.eq(monthNumber).parent().height($day.outerHeight());
		}
	};
})();

$(function () {
	App.clubCalendar = new App.ClubCalendar();
});
/* Modernizr 2.6.2 (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-fontface-backgroundsize-borderimage-borderradius-boxshadow-flexbox-hsla-multiplebgs-opacity-rgba-textshadow-cssanimations-csscolumns-generatedcontent-cssgradients-cssreflections-csstransforms-csstransforms3d-csstransitions-applicationcache-canvas-canvastext-draganddrop-hashchange-history-audio-video-indexeddb-input-inputtypes-localstorage-postmessage-sessionstorage-websockets-websqldatabase-webworkers-geolocation-inlinesvg-smil-svg-svgclippaths-touch-webgl-shiv-mq-cssclasses-addtest-prefixed-teststyles-testprop-testallprops-hasevent-prefixes-domprefixes-load
 */
;window.Modernizr=function(a,b,c){function D(a){j.cssText=a}function E(a,b){return D(n.join(a+";")+(b||""))}function F(a,b){return typeof a===b}function G(a,b){return!!~(""+a).indexOf(b)}function H(a,b){for(var d in a){var e=a[d];if(!G(e,"-")&&j[e]!==c)return b=="pfx"?e:!0}return!1}function I(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:F(f,"function")?f.bind(d||b):f}return!1}function J(a,b,c){var d=a.charAt(0).toUpperCase()+a.slice(1),e=(a+" "+p.join(d+" ")+d).split(" ");return F(b,"string")||F(b,"undefined")?H(e,b):(e=(a+" "+q.join(d+" ")+d).split(" "),I(e,b,c))}function K(){e.input=function(c){for(var d=0,e=c.length;d<e;d++)u[c[d]]=c[d]in k;return u.list&&(u.list=!!b.createElement("datalist")&&!!a.HTMLDataListElement),u}("autocomplete autofocus list placeholder max min multiple pattern required step".split(" ")),e.inputtypes=function(a){for(var d=0,e,f,h,i=a.length;d<i;d++)k.setAttribute("type",f=a[d]),e=k.type!=="text",e&&(k.value=l,k.style.cssText="position:absolute;visibility:hidden;",/^range$/.test(f)&&k.style.WebkitAppearance!==c?(g.appendChild(k),h=b.defaultView,e=h.getComputedStyle&&h.getComputedStyle(k,null).WebkitAppearance!=="textfield"&&k.offsetHeight!==0,g.removeChild(k)):/^(search|tel)$/.test(f)||(/^(url|email)$/.test(f)?e=k.checkValidity&&k.checkValidity()===!1:e=k.value!=l)),t[a[d]]=!!e;return t}("search tel url email datetime date month week time datetime-local number range color".split(" "))}var d="2.6.2",e={},f=!0,g=b.documentElement,h="modernizr",i=b.createElement(h),j=i.style,k=b.createElement("input"),l=":)",m={}.toString,n=" -webkit- -moz- -o- -ms- ".split(" "),o="Webkit Moz O ms",p=o.split(" "),q=o.toLowerCase().split(" "),r={svg:"http://www.w3.org/2000/svg"},s={},t={},u={},v=[],w=v.slice,x,y=function(a,c,d,e){var f,i,j,k,l=b.createElement("div"),m=b.body,n=m||b.createElement("body");if(parseInt(d,10))while(d--)j=b.createElement("div"),j.id=e?e[d]:h+(d+1),l.appendChild(j);return f=["&#173;",'<style id="s',h,'">',a,"</style>"].join(""),l.id=h,(m?l:n).innerHTML+=f,n.appendChild(l),m||(n.style.background="",n.style.overflow="hidden",k=g.style.overflow,g.style.overflow="hidden",g.appendChild(n)),i=c(l,a),m?l.parentNode.removeChild(l):(n.parentNode.removeChild(n),g.style.overflow=k),!!i},z=function(b){var c=a.matchMedia||a.msMatchMedia;if(c)return c(b).matches;var d;return y("@media "+b+" { #"+h+" { position: absolute; } }",function(b){d=(a.getComputedStyle?getComputedStyle(b,null):b.currentStyle)["position"]=="absolute"}),d},A=function(){function d(d,e){e=e||b.createElement(a[d]||"div"),d="on"+d;var f=d in e;return f||(e.setAttribute||(e=b.createElement("div")),e.setAttribute&&e.removeAttribute&&(e.setAttribute(d,""),f=F(e[d],"function"),F(e[d],"undefined")||(e[d]=c),e.removeAttribute(d))),e=null,f}var a={select:"input",change:"input",submit:"form",reset:"form",error:"img",load:"img",abort:"img"};return d}(),B={}.hasOwnProperty,C;!F(B,"undefined")&&!F(B.call,"undefined")?C=function(a,b){return B.call(a,b)}:C=function(a,b){return b in a&&F(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=w.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(w.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(w.call(arguments)))};return e}),s.flexbox=function(){return J("flexWrap")},s.canvas=function(){var a=b.createElement("canvas");return!!a.getContext&&!!a.getContext("2d")},s.canvastext=function(){return!!e.canvas&&!!F(b.createElement("canvas").getContext("2d").fillText,"function")},s.webgl=function(){return!!a.WebGLRenderingContext},s.touch=function(){var c;return"ontouchstart"in a||a.DocumentTouch&&b instanceof DocumentTouch?c=!0:y(["@media (",n.join("touch-enabled),("),h,")","{#modernizr{top:9px;position:absolute}}"].join(""),function(a){c=a.offsetTop===9}),c},s.geolocation=function(){return"geolocation"in navigator},s.postmessage=function(){return!!a.postMessage},s.websqldatabase=function(){return!!a.openDatabase},s.indexedDB=function(){return!!J("indexedDB",a)},s.hashchange=function(){return A("hashchange",a)&&(b.documentMode===c||b.documentMode>7)},s.history=function(){return!!a.history&&!!history.pushState},s.draganddrop=function(){var a=b.createElement("div");return"draggable"in a||"ondragstart"in a&&"ondrop"in a},s.websockets=function(){return"WebSocket"in a||"MozWebSocket"in a},s.rgba=function(){return D("background-color:rgba(150,255,150,.5)"),G(j.backgroundColor,"rgba")},s.hsla=function(){return D("background-color:hsla(120,40%,100%,.5)"),G(j.backgroundColor,"rgba")||G(j.backgroundColor,"hsla")},s.multiplebgs=function(){return D("background:url(https://),url(https://),red url(https://)"),/(url\s*\(.*?){3}/.test(j.background)},s.backgroundsize=function(){return J("backgroundSize")},s.borderimage=function(){return J("borderImage")},s.borderradius=function(){return J("borderRadius")},s.boxshadow=function(){return J("boxShadow")},s.textshadow=function(){return b.createElement("div").style.textShadow===""},s.opacity=function(){return E("opacity:.55"),/^0.55$/.test(j.opacity)},s.cssanimations=function(){return J("animationName")},s.csscolumns=function(){return J("columnCount")},s.cssgradients=function(){var a="background-image:",b="gradient(linear,left top,right bottom,from(#9f9),to(white));",c="linear-gradient(left top,#9f9, white);";return D((a+"-webkit- ".split(" ").join(b+a)+n.join(c+a)).slice(0,-a.length)),G(j.backgroundImage,"gradient")},s.cssreflections=function(){return J("boxReflect")},s.csstransforms=function(){return!!J("transform")},s.csstransforms3d=function(){var a=!!J("perspective");return a&&"webkitPerspective"in g.style&&y("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}",function(b,c){a=b.offsetLeft===9&&b.offsetHeight===3}),a},s.csstransitions=function(){return J("transition")},s.fontface=function(){var a;return y('@font-face {font-family:"font";src:url("https://")}',function(c,d){var e=b.getElementById("smodernizr"),f=e.sheet||e.styleSheet,g=f?f.cssRules&&f.cssRules[0]?f.cssRules[0].cssText:f.cssText||"":"";a=/src/i.test(g)&&g.indexOf(d.split(" ")[0])===0}),a},s.generatedcontent=function(){var a;return y(["#",h,"{font:0/0 a}#",h,':after{content:"',l,'";visibility:hidden;font:3px/1 a}'].join(""),function(b){a=b.offsetHeight>=3}),a},s.video=function(){var a=b.createElement("video"),c=!1;try{if(c=!!a.canPlayType)c=new Boolean(c),c.ogg=a.canPlayType('video/ogg; codecs="theora"').replace(/^no$/,""),c.h264=a.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/,""),c.webm=a.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/,"")}catch(d){}return c},s.audio=function(){var a=b.createElement("audio"),c=!1;try{if(c=!!a.canPlayType)c=new Boolean(c),c.ogg=a.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/,""),c.mp3=a.canPlayType("audio/mpeg;").replace(/^no$/,""),c.wav=a.canPlayType('audio/wav; codecs="1"').replace(/^no$/,""),c.m4a=(a.canPlayType("audio/x-m4a;")||a.canPlayType("audio/aac;")).replace(/^no$/,"")}catch(d){}return c},s.localstorage=function(){try{return localStorage.setItem(h,h),localStorage.removeItem(h),!0}catch(a){return!1}},s.sessionstorage=function(){try{return sessionStorage.setItem(h,h),sessionStorage.removeItem(h),!0}catch(a){return!1}},s.webworkers=function(){return!!a.Worker},s.applicationcache=function(){return!!a.applicationCache},s.svg=function(){return!!b.createElementNS&&!!b.createElementNS(r.svg,"svg").createSVGRect},s.inlinesvg=function(){var a=b.createElement("div");return a.innerHTML="<svg/>",(a.firstChild&&a.firstChild.namespaceURI)==r.svg},s.smil=function(){return!!b.createElementNS&&/SVGAnimate/.test(m.call(b.createElementNS(r.svg,"animate")))},s.svgclippaths=function(){return!!b.createElementNS&&/SVGClipPath/.test(m.call(b.createElementNS(r.svg,"clipPath")))};for(var L in s)C(s,L)&&(x=L.toLowerCase(),e[x]=s[L](),v.push((e[x]?"":"no-")+x));return e.input||K(),e.addTest=function(a,b){if(typeof a=="object")for(var d in a)C(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,typeof f!="undefined"&&f&&(g.className+=" "+(b?"":"no-")+a),e[a]=b}return e},D(""),i=k=null,function(a,b){function k(a,b){var c=a.createElement("p"),d=a.getElementsByTagName("head")[0]||a.documentElement;return c.innerHTML="x<style>"+b+"</style>",d.insertBefore(c.lastChild,d.firstChild)}function l(){var a=r.elements;return typeof a=="string"?a.split(" "):a}function m(a){var b=i[a[g]];return b||(b={},h++,a[g]=h,i[h]=b),b}function n(a,c,f){c||(c=b);if(j)return c.createElement(a);f||(f=m(c));var g;return f.cache[a]?g=f.cache[a].cloneNode():e.test(a)?g=(f.cache[a]=f.createElem(a)).cloneNode():g=f.createElem(a),g.canHaveChildren&&!d.test(a)?f.frag.appendChild(g):g}function o(a,c){a||(a=b);if(j)return a.createDocumentFragment();c=c||m(a);var d=c.frag.cloneNode(),e=0,f=l(),g=f.length;for(;e<g;e++)d.createElement(f[e]);return d}function p(a,b){b.cache||(b.cache={},b.createElem=a.createElement,b.createFrag=a.createDocumentFragment,b.frag=b.createFrag()),a.createElement=function(c){return r.shivMethods?n(c,a,b):b.createElem(c)},a.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+l().join().replace(/\w+/g,function(a){return b.createElem(a),b.frag.createElement(a),'c("'+a+'")'})+");return n}")(r,b.frag)}function q(a){a||(a=b);var c=m(a);return r.shivCSS&&!f&&!c.hasCSS&&(c.hasCSS=!!k(a,"article,aside,figcaption,figure,footer,header,hgroup,nav,section{display:block}mark{background:#FF0;color:#000}")),j||p(a,c),a}var c=a.html5||{},d=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,e=/^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,f,g="_html5shiv",h=0,i={},j;(function(){try{var a=b.createElement("a");a.innerHTML="<xyz></xyz>",f="hidden"in a,j=a.childNodes.length==1||function(){b.createElement("a");var a=b.createDocumentFragment();return typeof a.cloneNode=="undefined"||typeof a.createDocumentFragment=="undefined"||typeof a.createElement=="undefined"}()}catch(c){f=!0,j=!0}})();var r={elements:c.elements||"abbr article aside audio bdi canvas data datalist details figcaption figure footer header hgroup mark meter nav output progress section summary time video",shivCSS:c.shivCSS!==!1,supportsUnknownElements:j,shivMethods:c.shivMethods!==!1,type:"default",shivDocument:q,createElement:n,createDocumentFragment:o};a.html5=r,q(b)}(this,b),e._version=d,e._prefixes=n,e._domPrefixes=q,e._cssomPrefixes=p,e.mq=z,e.hasEvent=A,e.testProp=function(a){return H([a])},e.testAllProps=J,e.testStyles=y,e.prefixed=function(a,b,c){return b?J(a,b,c):J(a,"pfx")},g.className=g.className.replace(/(^|\s)no-js(\s|$)/,"$1$2")+(f?" js "+v.join(" "):""),e}(this,this.document),function(a,b,c){function d(a){return"[object Function]"==o.call(a)}function e(a){return"string"==typeof a}function f(){}function g(a){return!a||"loaded"==a||"complete"==a||"uninitialized"==a}function h(){var a=p.shift();q=1,a?a.t?m(function(){("c"==a.t?B.injectCss:B.injectJs)(a.s,0,a.a,a.x,a.e,1)},0):(a(),h()):q=0}function i(a,c,d,e,f,i,j){function k(b){if(!o&&g(l.readyState)&&(u.r=o=1,!q&&h(),l.onload=l.onreadystatechange=null,b)){"img"!=a&&m(function(){t.removeChild(l)},50);for(var d in y[c])y[c].hasOwnProperty(d)&&y[c][d].onload()}}var j=j||B.errorTimeout,l=b.createElement(a),o=0,r=0,u={t:d,s:c,e:f,a:i,x:j};1===y[c]&&(r=1,y[c]=[]),"object"==a?l.data=c:(l.src=c,l.type=a),l.width=l.height="0",l.onerror=l.onload=l.onreadystatechange=function(){k.call(this,r)},p.splice(e,0,u),"img"!=a&&(r||2===y[c]?(t.insertBefore(l,s?null:n),m(k,j)):y[c].push(l))}function j(a,b,c,d,f){return q=0,b=b||"j",e(a)?i("c"==b?v:u,a,b,this.i++,c,d,f):(p.splice(this.i++,0,a),1==p.length&&h()),this}function k(){var a=B;return a.loader={load:j,i:0},a}var l=b.documentElement,m=a.setTimeout,n=b.getElementsByTagName("script")[0],o={}.toString,p=[],q=0,r="MozAppearance"in l.style,s=r&&!!b.createRange().compareNode,t=s?l:n.parentNode,l=a.opera&&"[object Opera]"==o.call(a.opera),l=!!b.attachEvent&&!l,u=r?"object":l?"script":"img",v=l?"script":u,w=Array.isArray||function(a){return"[object Array]"==o.call(a)},x=[],y={},z={timeout:function(a,b){return b.length&&(a.timeout=b[0]),a}},A,B;B=function(a){function b(a){var a=a.split("!"),b=x.length,c=a.pop(),d=a.length,c={url:c,origUrl:c,prefixes:a},e,f,g;for(f=0;f<d;f++)g=a[f].split("="),(e=z[g.shift()])&&(c=e(c,g));for(f=0;f<b;f++)c=x[f](c);return c}function g(a,e,f,g,h){var i=b(a),j=i.autoCallback;i.url.split(".").pop().split("?").shift(),i.bypass||(e&&(e=d(e)?e:e[a]||e[g]||e[a.split("/").pop().split("?")[0]]),i.instead?i.instead(a,e,f,g,h):(y[i.url]?i.noexec=!0:y[i.url]=1,f.load(i.url,i.forceCSS||!i.forceJS&&"css"==i.url.split(".").pop().split("?").shift()?"c":c,i.noexec,i.attrs,i.timeout),(d(e)||d(j))&&f.load(function(){k(),e&&e(i.origUrl,h,g),j&&j(i.origUrl,h,g),y[i.url]=2})))}function h(a,b){function c(a,c){if(a){if(e(a))c||(j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}),g(a,j,b,0,h);else if(Object(a)===a)for(n in m=function(){var b=0,c;for(c in a)a.hasOwnProperty(c)&&b++;return b}(),a)a.hasOwnProperty(n)&&(!c&&!--m&&(d(j)?j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}:j[n]=function(a){return function(){var b=[].slice.call(arguments);a&&a.apply(this,b),l()}}(k[n])),g(a[n],j,b,n,h))}else!c&&l()}var h=!!a.test,i=a.load||a.both,j=a.callback||f,k=j,l=a.complete||f,m,n;c(h?a.yep:a.nope,!!i),i&&c(i)}var i,j,l=this.yepnope.loader;if(e(a))g(a,0,l,0);else if(w(a))for(i=0;i<a.length;i++)j=a[i],e(j)?g(j,0,l,0):w(j)?B(j):Object(j)===j&&h(j,l);else Object(a)===a&&h(a,l)},B.addPrefix=function(a,b){z[a]=b},B.addFilter=function(a){x.push(a)},B.errorTimeout=1e4,null==b.readyState&&b.addEventListener&&(b.readyState="loading",b.addEventListener("DOMContentLoaded",A=function(){b.removeEventListener("DOMContentLoaded",A,0),b.readyState="complete"},0)),a.yepnope=k(),a.yepnope.executeStack=h,a.yepnope.injectJs=function(a,c,d,e,i,j){var k=b.createElement("script"),l,o,e=e||B.errorTimeout;k.src=a;for(o in d)k.setAttribute(o,d[o]);c=j?h:c||f,k.onreadystatechange=k.onload=function(){!l&&g(k.readyState)&&(l=1,c(),k.onload=k.onreadystatechange=null)},m(function(){l||(l=1,c(1))},e),i?k.onload():n.parentNode.insertBefore(k,n)},a.yepnope.injectCss=function(a,c,d,e,g,i){var e=b.createElement("link"),j,c=i?h:c||f;e.href=a,e.rel="stylesheet",e.type="text/css";for(j in d)e.setAttribute(j,d[j]);g||(n.parentNode.insertBefore(e,n),m(c,0))}}(this,document),Modernizr.load=function(){yepnope.apply(window,[].slice.call(arguments,0))};


$(document).on('click', '.form-footer .btn' , function(e){
    var form = $('.form-footer');
    form.find('.l-screen-form__row').addClass('error');
    form.find('.l-screen-form__row_button').removeClass('error');
    form.find('.error-text').remove();
    var text = 'Текст описание ошибки, раздвигает формы на 20 пикслей вниз';
    $.each(form.find('input'),function(i,index){
        //console.log($(this) , i , index);
        if(!$(this).hasClass('btn'))
            $(this).after('<span class="error-text">' + text + '</span>');
    })
    $.each(form.find('.customSelect'),function(i,index){
        //console.log($(this) , i , index);
            $(this).after('<span class="error-text">' + text + '</span>');
    })
});
var isIPad = isUserAgentCheck(["iPad"]),
    isIPhone = isUserAgentCheck(["iPhone", "iPod", "blackberry", "android", "CriOS", "Mobile", "Windows Phone", "IEMobile"]) && !isUserAgentCheck(["iPad"]),
    isAndroid = isUserAgentCheck(["android"]),
    isMobile = isIPad || isIPhone || isAndroid,
    isIE8 = isUserAgentCheck(["MSIE 8.0"]),
    isIE9 = isUserAgentCheck(["MSIE 9.0"]),
    isOldIE = isIE8 || isIE9,
    noSwiffyAnim = isMobile || isIE8 || isUserAgentCheck(["MSIE 7.0"]) || isUserAgentCheck(["MSIE 6.0"]),
    noAudio = isMobile,
    noTransition = isIE8 || isIE9 || isUserAgentCheck(["Opera"]) || isUserAgentCheck(["MSIE 7.0"]) || isUserAgentCheck(["MSIE 6.0"]),
    noAnimation = isIE8 || isUserAgentCheck(["Opera"]) || isUserAgentCheck(["MSIE 7.0"]) || isUserAgentCheck(["MSIE 6.0"]),
    flashVideo = isIE8 || isUserAgentCheck(["Opera"]) || isUserAgentCheck(["MSIE 7.0"]) || isUserAgentCheck(["MSIE 6.0"]),
    Orientation = '';

(function () {
    var method;
    var noop = function () {
    };
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

if (!window.location.origin) window.location.origin = window.location.protocol + "//" + window.location.host;
if (!document.location.origin) document.location.origin = window.location.origin;
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (elt, from) {
        var len = this.length >>> 0;

        var from = Number(arguments[1]) || 0;
        from = (from < 0)
            ? Math.ceil(from)
            : Math.floor(from);
        if (from < 0)
            from += len;

        for (; from < len; from++) {
            if (from in this &&
                this[from] === elt)
                return from;
        }
        return -1;
    };
}

if (!Array.prototype.indexOfByObjectField) {
    Array.prototype.indexOfByObjectField = function (elt, from) {
        var len = this.length >>> 0,
            field = Object.keys(elt);

        var from = Number(arguments[1]) || 0;
        from = (from < 0)
            ? Math.ceil(from)
            : Math.floor(from);
        if (from < 0)
            from += len;

        for (; from < len; from++) {
            if (from in this &&
                this[from][field[0]] === elt[field[0]])
                return from;
        }
        return -1;
    };
}

if (!Array.prototype.filter)
{
    Array.prototype.filter = function(fun /*, thisArg */)
    {
        "use strict";

        if (this === void 0 || this === null)
            throw new TypeError();

        var t = Object(this);
        var len = t.length >>> 0;
        if (typeof fun != "function")
            throw new TypeError();

        var res = [];
        var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
        for (var i = 0; i < len; i++)
        {
            if (i in t)
            {
                var val = t[i];

                // NOTE: Technically this should Object.defineProperty at
                //       the next index, as push can be affected by
                //       properties on Object.prototype and Array.prototype.
                //       But that method's new, and collisions should be
                //       rare, so use the more-compatible alternative.
                if (fun.call(thisArg, val, i, t))
                    res.push(val);
            }
        }

        return res;
    };
}

if (!Array.prototype.unique) {
    Array.prototype.unique = function() {
        return this.filter(function(element, index, arr) {
            return this.indexOf(element, index+1) === -1;
        }, this);
    };
}

function parseBool(value){
    return (typeof value === "undefined") ? false : value.replace(/^\s+|\s+$/g, "").toLowerCase() === "true";
}

function compareObjects(o1, o2) {
    var keys1 = Object.keys(o1), keys2 = Object.keys(o2), keysLen1 = keys1.length, keysLen2 = keys2.length;
    if (keysLen1 !== keysLen2) return false;
    else {
        for (var i = 0; i < keysLen1; i++) if (o1[keys1[i]] !== o2[keys1[i]]) return false;
        return true;
    }

}

window.requestAnimFrame =
    window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function (callback) {
            window.setTimeout(callback, 1000 / 60);
        };

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function numberInRange(n, min, max) {
    return Math.max(min, Math.min(max, n));
}

function isUserAgentCheck(ua) {
    var agent = navigator.userAgent;
    for (var i = 0; i < ua.length; i++)
        if (Boolean(agent.match(new RegExp(ua[i], "i"))))
            return true;
    return false;
}

function loadCss(url, id) {
    var link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = url;
    link.id = id;
    document.getElementsByTagName("head")[0].appendChild(link);
}

function removeCss(id) {
    $('#' + id).remove();
}

var ease = {
    line: "linear",
    In: "ease-in",
    Out: "cubic-bezier(0.215, 0.610, 0.355, 1.000)",
    OutCirc: "cubic-bezier(0.075, 0.820, 0.165, 1.000)",
    InOut: "ease-in-out",
    InOutBack: "cubic-bezier(0.680, -0.550, 0.265, 1.550);"
}

function ianIt(selector, css) {
    var _css = css;
    var selectorArr = [];

    if ((selector != null) && (selector.length > 0) && (typeof selector !== 'string')) selectorArr = selector;
    else selectorArr.push(selector);

    for (var i = 0; i < selectorArr.length; i++)
        $(selectorArr[i]).css(_css);
}

var anItQueue = [], anItQueueSelectors = [];
var QueueIsWork=false;
function anIt(selector, css, time, delay, easing, _callback) {
    anItQueue.push({
        selector: selector,
        css: css,
        time: time,
        delay: delay,
        easing: easing,
        _callback: _callback
    });

    if (!QueueIsWork) applyAnItQueue();
}

function applyAnItQueue(){
    //console.log('animation count at the moment: '+anItQueue.length);

  /*  for (var i=0;i<anItQueue.length;i++){
        console.log('element selector: '+anItQueue[i].selector);
    }*/



    QueueIsWork=true;
    requestAnimFrame(function () {

      // setTimeout(function(){
            var qItem = anItQueue.shift();
          // console.log('++++++++++++++++++++++++++++++',qItem.selector,qItem.css);
            _anIt(qItem.selector, qItem.css, qItem.time, qItem.delay, qItem.easing, qItem._callback);

            if (anItQueue.length!==0) applyAnItQueue();
            else QueueIsWork=false;
       //},300);

    });
}
function _anIt(selector, css, time, delay, easing, _callback) {

    var applyTransitionTime=50,
        selectorArr = [],selectorArrLen,
        clearLastCallback = (typeof css.noClearLastCallback !== 'undefined') ? !css.noClearLastCallback : true,
        callback = (typeof _callback !== 'undefined') ? _callback : function () {},
        prevTimeOutId=null,
        i=0;

    if ((selector != null) && (selector.length > 0) && (typeof selector !== 'string')) selectorArr = selector;
    else selectorArr.push(selector);

    if (Modernizr.csstransitions) {
        var _css = css,_cssTransition={},_css_arr = Object.keys(css),_css_arr_len=_css_arr.length;
        _cssTransition["transition-duration"] = time + 'ms';
        _cssTransition["transition-timing-function"] = easing;

        _cssTransition["transition-property"] = '';

        for(i=0;i<_css_arr_len;i++){
            if (_css_arr[i]!=='noClearLastCallback') _cssTransition["transition-property"]+=_css_arr[i];
            if (i<(_css_arr_len-1)) _cssTransition["transition-property"]+=', ';
        }

        var to = setTimeout(function () {
            selectorArrLen=selectorArr.length;
            var cto = setTimeout(function () {
                _cssTransition = {};
                _cssTransition["transition-duration"] = '';
                _cssTransition["transition-timing-function"] = '';
                _cssTransition["transition-property"] = '';
                for (i = 0; i < selectorArrLen; i++)
                    $(selectorArr[i]).css(_cssTransition);

                callback();

            }, time + applyTransitionTime);

            for (i = 0; i < selectorArrLen; i++) {
                var $item = $(selectorArr[i]);

                if (clearLastCallback) {
                    prevTimeOutId = $item.attr('data-aito');
                    clearTimeout(prevTimeOutId);
                }

               ///console.log(_cssTransition);

                $item.css(_cssTransition).attr({'data-aito': cto});

                setTimeout(function(){
                    $item.css(_css);
                },applyTransitionTime);
            }

        }, delay);

    } else {
        selectorArrLen=selectorArr.length;
        for (var i = 0; i < selectorArrLen; i++)
            $(selectorArr[i]).css(css);

        callback();
    }
}
function animateCss(sel,animName,callback) { //animate.css
    $(sel).removeClass().addClass(animName + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        callback();
    });
};
function animateIt(selector, css, time, easing, delay, callback) {
    if (ieNum > 9) {
        var _css = css;
        var _css_arr = Object.keys(css);

        _css['-webkit-transition'] = '';
        _css['transition'] = '';
        for (var i = 0; i < _css_arr.length; i++) {
            _css['-webkit-transition'] += ((i > 0) ? ', ' : '') + _css_arr[i] + ' ' + time + 'ms ' + ((easing == '') ? 'cubic-bezier(0.215, 0.61, 0.355, 1)' : easing);
            _css['transition'] += ((i > 0) ? ', ' : '') + _css_arr[i] + ' ' + time + 'ms ' + ((easing == '') ? 'cubic-bezier(0.215, 0.61, 0.355, 1)' : easing);
        }

        setTimeout(function () {
            $(selector).css(_css);

            setTimeout(function () {
                $(selector).css({ '-webkit-transition' : '', 'transition' : '' });
                if (typeof callback === 'function') { callback(); }
            }, time + 50);
        }, delay + 50);
    } else {
        $(selector).css(css);
        if (typeof callback === 'function') { callback(); }
    }
}

function onBlock(selector) {
    $(selector).css({'z-index': '100'})
}

function offBlock(selector) {
    $(selector).css({'z-index': '-100'})
}


function switchBlocks(state, arr) {
    //console.log(state, arr)
    var al = arr.length, i = 0, $item = null,
        cssVisible = {
            'visibility': 'visible',
            'display': 'block'
        },
        cssHidden = {
            'visibility': 'hidden',
            'display': 'none'
        };


    for (i = 0; i < al; i++) {
        $item = $(arr[i]);

        if (state == 'show') {
          /*  $item.css(cssVisible).addClass('blockVisible').removeClass('blockHidden');
           // if (($item.css('display')==='none')&&(typeof window.triggerResize !== 'undefined'))
            if (typeof window.triggerResize !== 'undefined')
                window.triggerResize();*/
            $item.show().addClass('blockVisible').removeClass('blockHidden');
            if (typeof window.triggerResize !== 'undefined')
                window.triggerResize();
        }
        else if (state == 'hide') {
            $item.css(cssHidden).removeClass('blockVisible').addClass('blockHidden');
            //$item.find('.blockVisible').css(cssHidden).addClass('blockHiddenByParent');
            $item.hide().removeClass('blockVisible').addClass('blockHidden');
        }

    }

    //$(window).trigger('resize');
}


function switchBlocks2(state, arr) {
    var al = arr.length;
    var i = 0;
    var pr;
    var display = 'none';
    if (($.browser.msie) && ($.browser.version < 9)) {
        display = 'block';
    }

    for (i = 0; i < arr.length; i++)
        if (state == 'show') {
            $(arr[i]).css({
                'transform': 'scale(1)',
                'display': 'block', 'visibility': 'visible'}).addClass('blockVisible').removeClass('blockHidden')
            // $(window).trigger('resize');
            //$(arr[i]).trigger('resize');

            if (typeof $(arr[i])[0] !== 'undefined') {
                pr = $(arr[i])[0].style.top;
            }

        }
        else if (state == 'hide') {
            $(arr[i]).css({
                'transform': 'scale(0)',
                'display': 'none', 'visibility': 'hidden'}).removeClass('blockVisible').addClass('blockHidden')
        }

    $(window).trigger('resize');
}

/*

 function switchBlocks(state, arr) {
 var al = arr.length;
 var i = 0;
 for (i = 0; i < arr.length; i++)
 if (state == 'show') $(arr[i]).css({'visibility': 'visible'}).addClass('blockVisible');
 else if (state == 'hide') $(arr[i]).css({'visibility': 'hidden'}).removeClass('blockVisible');
 }*/
function replaceMainBg(src) {
    var newBg = new Image();
    $('#mainBGS').append('<img src="' + src + '" class="bg bgHidden" data-w="1600" data-h="900"/>');

    $(newBg).load(function () {
        anIt('.bgHidden', {'opacity': '1'}, 300, 0, 'linear', function () {
            $('.bgVisible').remove();
            $('.bgHidden').addClass('bgVisible').removeClass('bgHidden');
        });
    });
    /*$(newBg).error(function () {
     $('.bgHidden').remove();
     });*/
    newBg.src = src;
}

function loadImg(src, domObj, onload, error) {
    var newBg = new Image();
    $(newBg).load(function () {
        onload(domObj);
    });
    $(newBg).error(function () {
        error(domObj);
    });
    newBg.src = src;
}
function CookieFlag(cookieName) {

    var _flag = null;
    getCookie();

    function getCookie() {

        if (cookieName !== "undefined") {
            setFlag(getCookieState());
        } else setFlag(false);
    }

    function getCookieState() {
        var c_name = cookieName;

        if (document.cookie.length > 0) {
            var c_start = document.cookie.indexOf(c_name + "=");
            if (c_start != -1) {
                c_start = c_start + c_name.length + 1;
                var c_end = document.cookie.indexOf(";", c_start);
                if (c_end == -1) c_end = document.cookie.length;

                if (unescape(document.cookie.substring(c_start, c_end)) == 'true') return true;
                else return false;
            }
        }
        return false;
    }

    function toogleFlag() {
        setFlag(!_flag);
    }

    function setFlag(flag) {
        _flag = flag;
        if (document.cookie && (cookieName !== "undefined")) {
            document.cookie = cookieName + '=' + _flag + ';';
        }
    }

    function getFlag() {
        return _flag;
    }

    return{
        setFlag: setFlag,
        getFlag: getFlag,
        toogleFlag: toogleFlag
    }
}

function buildSeq(obj, fc, path, f_type, size) {
    var as = '';
    for (var i = 0; i < fc; i++) {
        as = '<img src="' + path + i + '.' + f_type + '" data-w="' + size.w + '" data-h="' + size.h + '"/>';
        obj.append(as);
    }
}

function play_seq(obj, freq, callback) {
    //obj.css({'visibility': 'visible'});
    var img_arr = obj.children('img').removeClass('on');
    var seq_l = img_arr.length;

    var as = '';
    for (var i = 0; i < seq_l; i++) {
        on_img(img_arr, i, seq_l)
    }


    function on_img(arr, index, lenght) {
        setTimeout(function () {
            if (index > 0) $(arr[index - 1]).removeClass('on');
            $(arr[index]).addClass('on');

            if (index == (lenght - 1)) {
                callback();
                //obj.css({'visibility': 'hidden'});
                //$(arr[index]).removeClass('on');
            }

        }, freq * index)
    }
}

function playBack_seq(obj, freq, callback) {
    //obj.css({'visibility': 'visible'});
    var img_arr = obj.children('img').removeClass('on');
    var seq_l = img_arr.length;

    var as = '';
    for (var i = 0; i < seq_l; i++) {
        on_img(img_arr, i, seq_l)
    }


    function on_img(arr, index, lenght) {
        setTimeout(function () {
            if (index < lenght) $(arr[index + 1]).removeClass('on');
            $(arr[index]).addClass('on');

            if (index == 0) {
                callback();
                //obj.css({'visibility': 'hidden'});
                //$(arr[index]).removeClass('on');
            }

        }, freq * (lenght - index))
    }
}

function cubeSlider(dir, orientation, next, now, cont, callback) {
    var animBaseTime = 500;

    var OriginNow, OriginNext;

    if (orientation[0] == 'X') {
        OriginNow = (dir == 1) ? '100% 50%' : '0% 50%';
        OriginNext = (dir == 1) ? '0% 50%' : '100% 50%';
    } else if (orientation[0] == 'Y') {
        OriginNow = (dir == 1) ? '50% 100%' : '50% 0%';
        OriginNext = (dir == 1) ? '50% 0%' : '50% 100%';
    }

    ianIt(next, {'transform': 'translate' + orientation[0] + '(' + dir * 100 + '%) rotate' + orientation[1] + '(' + dir * 90 + 'deg)', 'transform-origin': OriginNext});
    ianIt(now, {'transform-origin': OriginNow});
    ianIt(cont, {'perspective': '1000px', 'transform-style': 'preserve-3d'});

    anIt(cont, {'transform': 'translateZ(-100px)'}, animBaseTime / 2, 0, ease.Out, function () {
        anIt(cont, {'transform': 'translateZ(0px)'}, animBaseTime / 2, 0, ease.Out, function () {
        });
    });

    anIt(next, {'transform': 'translate' + orientation[0] + '(0%) rotate' + orientation[1] + '(0deg)'}, animBaseTime, 0, ease.Out, function () {
    });
    anIt(now, {'transform': 'translate' + orientation[0] + '(' + (-1 * dir * 100) + '%) rotate' + orientation[1] + '(' + (-1 * dir * 90) + 'deg)'}, animBaseTime, 0, ease.Out, function () {

        callback();
    });
}
function fleepSlider(next, now, halfTime_cb, fullTime_cb) {
    var cont = $(next).parent();
    var animBaseTime = 900; ///900
    var perspective = 1500;


    ianIt(cont, { 'transform': 'perspective(' + perspective + 'px) scale(1) rotateY(0deg)'});
    ianIt(next, { 'transform': 'scale(0)'});

    switchBlocks('show', [next]);
    halfTime_cb();


    requestAnimFrame(function () {
        anIt(cont, {'transform': 'perspective(' + perspective + 'px) scale(0.7) rotateY(90deg)'}, animBaseTime / 2, 100, ease.In, function () {

            ianIt(next, {'transform': 'scale(-1,1)'});
            ianIt(now, {'transform': 'scale(0)'});

            requestAnimFrame(function () {
                anIt(cont, {'transform': 'perspective(' + perspective + 'px) scale(1) rotateY(180deg)'}, animBaseTime / 2, 0, ease.Out, function () {

                    ianIt([cont,next], {'transform': ''});

                    switchBlocks('hide', [now]);
                    ianIt(now, {'transform': ''});

                    fullTime_cb();
                });
            });
        });
    });
}

/*
 function fleepSlider(next, now, halfTime_cb, fullTime_cb) {
 var cont = $(next).parent();
 var animBaseTime = 800;
 var perspective = 1500;

 ianIt(cont, {'transform-style': 'preserve-3d', 'backface-visibility': 'hidden'});
 ianIt(next,{'margin-left': '10000px'});
 switchBlocks('show', [next]);
 anIt(cont, {'transform': 'perspective(' + perspective + 'px) scale3d(0.7, 0.7,0.7) rotate3d(0,1,0,90deg)'}, animBaseTime / 2, 0, ease.In, function () {
 halfTime_cb();
 if (next !== now) {
 switchBlocks('hide', [now]);
 ianIt(next,{'margin-left': ''});
 }
 ianIt(cont, {'transform': 'perspective(' + perspective + 'px) scale3d(0.5, 0.5,0.5) rotate3d(0,1,0,-90deg)'});
 anIt(cont, {'transform': 'perspective(' + perspective + 'px) scale3d(1, 1, 1) rotate3d(0,1,0,0deg)'}, animBaseTime / 2, 0, ease.Out, function () {
 fullTime_cb();
 ianIt(cont, {'transform': '', 'transform-style': '', 'backface-visibility': ''});
 });

 });
 }*/

function Fleep3dSimple(obj, type, _cb) {
    var cb = (typeof _cb !== "undefined") ? _cb : function () {
    };
    //switchBlocks('hide',[next]);
    var animBaseTime = 500;
    var perspective = 200;

    if (type == 'show') {
        //switchBlocks('show',[obj]);
        $(obj).show();
        anIt(obj, {'transform': 'perspective(' + perspective + 'px) scale3d(1, 1, 1) rotate3d(1,0,0,0deg) translate3d(0%,0%,0px)', opacity: 1}, animBaseTime, 0, ease.Out, function () {
            cb();
        });
    } else {
        anIt(obj, {'transform': 'perspective(' + perspective + 'px) scale3d(0.8, 0.8, 0.8) rotate3d(1,0,0,90deg) translate3d(0%,300%,0px)', opacity: 0}, animBaseTime, 0, ease.In, function () {
            cb();
            $(obj).hide();
            //switchBlocks('hide',[obj]);
        });
    }


}
/*
 function zoomSlider(next, now, callback) {

 var cont = $(next).parent();
 var animBaseTime = 500;

 ianIt(cont, {'transform-style': 'preserve-3d', 'backface-visibility': 'hidden'});
 ianIt(now, {'z-index': 900});
 switchBlocks('show', [next, now]);

 $(now).addClass('owl-scaleUp-out');
 $(next).addClass('owl-scaleUp-in');

 setTimeout(function(){
 callback();
 $(now).removeClass('owl-scaleUp-out');
 $(next).removeClass('owl-scaleUp-in');
 switchBlocks('hide', [now]);
 ianIt([next, now], {'transform': '', 'opacity': ''});
 ianIt(cont, {'transform-style': '', 'backface-visibility': ''});
 ianIt([now], {'z-index': ''});
 },animBaseTime);
 }*/
function zoomSlider(next, now, callback) {

    var cont = $(next).parent();
    var animBaseTime = 700;

    //ianIt(cont, {'transform-style': 'preserve-3d', 'backface-visibility': 'hidden'});
    ianIt(now, {'z-index': 900});
    ianIt(next, {'transform': 'scale(1.2)', 'opacity': 0, 'z-index': 1000});
    switchBlocks('show', [next, now]);

    anIt(now, {'transform': 'scale(1.2)'}, animBaseTime / 2, 0, ease.Out, function () {
    });
    anIt(next, {'transform': 'scale(1)', opacity: 1}, animBaseTime, 0, ease.Out, function () {
        //ianIt(cont, {'transform': '', 'transform-style': '', 'backface-visibility': ''});
        callback();
        switchBlocks('hide', [now]);
        ianIt([next, now], {'transform': '', 'opacity': ''});
        ianIt([now], {'z-index': ''});
    });
}

function zoomSlider2(next, now, callback) {

    var cont = $(next).parent();
    var animBaseTime = 700;

    ianIt(now, {'z-index': 900});
    ianIt(next, {'transform': 'scale(1.2)', 'opacity': 0, 'z-index': 1000});
    //switchBlocks('show', [next, now]);

    anIt(now, {'transform': 'scale(1.2)'}, animBaseTime / 2, 0, ease.Out, function () {
    });
    anIt(next, {'transform': 'scale(1)', opacity: 1}, animBaseTime, 0, ease.Out, function () {

        callback();
        ianIt(now, {'transform': '', 'opacity': 0});
        ianIt(next, {'transform': '', 'opacity': 1});
    });
}
function clearZoomSlider2(next, now) {

    //console.log(next, now)
    var cont = $(next).parent()
    //switchBlocks('hide',[next]);
    var animBaseTime = 0;
    var perspective = 3000;

    ianIt(cont, {'transform': '', 'transform-style': '', 'backface-visibility': ''});

    ianIt(now, {'transform': '', 'opacity': 0});
    ianIt(next, {'transform': '', 'opacity': 1});

}

function waitCondition(condition, callback) {
    /*   if (condition()) callback();
     else setTimeout(function () {
     waitCondition(condition, callback);
     }, 16);*/

    if (condition()) requestAnimFrame(callback);
    else requestAnimFrame(function () {
        waitCondition(condition, callback);
    });
}

function delay(i, delayTime, delayFunction) {
    setTimeout(function () {
        delayFunction(i);
    }, i * delayTime)
}

function fileGetJSONContents(url, completeCallback) {
    var success = true;
    var resultData = '';
    var result = $.Deferred();

    $.ajax({
        'url': url,
        'cache': true,
        'dataType': 'json',
        beforeSend: function (xhr) {
        },
        'success': function (fileData) {
            resultData = fileData;
        },
        'error': function () {
            success = false;
        },
        'complete': function () {
            if (completeCallback instanceof Function) completeCallback(success, resultData);
            result.resolveWith(window, [success, resultData]);
        }
    });
    return result.promise();
}


var swiffyAnim = {
    start: function (arr, time, curIdIndex) {
        if (!noSwiffyAnim) {
            this.initPlayAnimArrIcos(arr, time, curIdIndex)
        }
    },
    stop: function (arr, curIdIndex) {
        if (!noSwiffyAnim) {
            this.destroyAnimArrIcos(arr, curIdIndex)
        }
    },
    initPlayAnimArrIcos: function (arr, time, curIdIndex) {
        this.destroyAnimArrIcos(arr);
        var al = arr.length;
        var stagesArr = [];
        this.destroyAnimArrIcos(arr);
        arr['stages' + curIdIndex] = [];
        for (var j = 0; j < al; j++) {

            var sElem = document.getElementById(arr[j].cIds[curIdIndex]);
            //sElem.innerHTML='';
            $('#' + arr[j].cIds[curIdIndex]).html('');
            var stage = new swiffy.Stage(sElem, arr[j].swiffyobject);
            stage.setBackground(null);
            arr['stages' + curIdIndex].push(stage);
            _start(stage, j)
        }
        function _start(stage, index) {
            setTimeout(function () {
                stage.start();
            }, index * time);
        }
    },
    destroyAnimArrIcos: function (arr, curIdIndex) {

        var al = (typeof arr['stages' + curIdIndex] !== 'undefined') ? arr['stages' + curIdIndex].length : 0;
        var stagesArr = [];
        var flag = false;
        var dstart = 0;
        var dstop = 0;
        for (var i = 0; i < al; i++) {
            if (arr['stages' + curIdIndex][i].destroy) {
                if (!flag) {
                    dstart = i;
                    flag = true;
                }
                arr['stages' + curIdIndex][i].destroy();
                dstop++;
            }
        }
        try {
            arr['stages' + curIdIndex].splice(dstart, dstop);
        } catch (e) {
        }

    }
}
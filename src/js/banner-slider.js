
// fotorama settings

var fotorama = $('.fotorama').fotorama({
    width: '100%',
    height: 348,
    nav: 'dots',
    arrows: false,
    allowfullscreen: false
});

var bg = $('.banners');
fotorama.on('fotorama:show' , function(e, fotorama){
    var index = fotorama.activeIndex + 1;
    var color = bg.find('.slide-' + index).attr('data-color');
    bg.css({
        'background-color' : '#' + color
    });
});

fotorama.on(function (e, fotorama, extra) {
    //console.log('## ' + e.type);
    //console.log('active frame', fotorama.activeFrame);
    //console.log('additional data', extra);
});


var zoomValue = 17;
var mapCanvas = document.getElementById('office-map');
var myLatLng = new google.maps.LatLng(55.7548683,37.6344741);

var styles =[
    {
        featureType: "all",
        elementType: "all",
        stylers: [
            { saturation: -100 } // <-- THIS
        ]
    }
];

var mapOptions = {
    center: myLatLng,
    zoom: zoomValue,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    disableDefaultUI: true,
    styles: styles
}

var map = new google.maps.Map(mapCanvas, mapOptions);

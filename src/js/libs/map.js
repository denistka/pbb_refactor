/************************************
 *
 *   Map ( alpha 0.2 )
 *
 *   SETTINGS:
 *
 *   String wrapper - id блока c картой ( default : 'map' )
 *   String icon - ссылка на иконку маркера
 *   Object center - объект ( lat , lng ) , координаты центра карты ( default : координаты Москвы )
 *   Number zoom - зум карты ( default : 8 )
 *   Number minZoom - минимальный зум карты ( default : 2 )
 *   Number maxZoom - максимальый зум карты ( default : 18 )
 *   Array arrayMarkers - массив с маркерами
 *   Array styles - массив со стилями
 *   Boolean scrollwheel - скол ( default : 'true' )    GoogleMap Option
 *   Boolean navigationControl  ( default : 'true' )    GoogleMap Option
 *   Boolean mapTypeControl - выбор типа карты ( default : 'true' ) GoogleMap Option
 *   Boolean scaleControl   ( default : 'true' )    GoogleMap Option
 *   Boolean disableDefaultUI - отключение интерфейса гугл карт ( default : 'false' )    GoogleMap Option
 *   Function callback -  функция callback
 *
 *
 *   info link https://developers.google.com/maps/documentation/javascript/reference?hl=ru#MapOptions
 *
 *
 *   FUNCTIONS:
 *
 *   Obj.searchByAddress(String) - функция поиска по адрессу
 *
 ************************************/

function Map(settings) {


    var wrapper = (settings && settings.wrapper) ? settings.wrapper : 'map';
    var icon = (settings && settings.icon) ? settings.icon : '';
    var center = (settings && settings.center) ? settings.center : '';
    var zoom = (settings && settings.zoom) ? settings.zoom : 8;
    var minZoom = (settings && settings.minZoom) ? settings.minZoom : 2;
    var maxZoom = (settings && settings.maxZoom) ? settings.maxZoom : 18;
    var arrayMarkers = (settings && settings.arrayMarkers) ? settings.arrayMarkers : false;
    var clickEvent = (settings && settings.clickEvent) ? settings.clickEvent : false;
    var infoWindow = (settings && settings.infoWindow) ? settings.infoWindow : false;
    var infoBox = (settings && settings.infoBox) ? settings.infoBox : true;
    var appendBlock = (settings && settings.appendBlock) ? settings.appendBlock : false;
    var styles = (settings && settings.styles) ? settings.styles : false;
    var scrollwheel = (settings && (typeof settings.scrollwheel != 'undefined')) ? settings.scrollwheel : true;
    var navigationControl = (settings && (typeof settings.navigationControl != 'undefined' )) ? settings.navigationControl : true;
    var mapTypeControl = (settings && (typeof settings.mapTypeControl != 'undefined')) ? settings.mapTypeControl : true;
    var scaleControl = (settings && (typeof settings.scaleControl != 'undefined')) ? settings.scaleControl : true;
    var disableDefaultUI = (settings && (typeof settings.disableDefaultUI != 'undefined')) ? settings.disableDefaultUI : false;
    var callback = (settings && settings.callback) ? settings.callback : function (data) {
    };
    var reInit;
//  Инициализация
    this.init = function (reInit) {
        if (typeof reInit != 'undefined') {
            center = {
                'lat': reInit.center.lat,
                'lng': reInit.center.lng
            }
            zoom = reInit.zoom;
        }
        if (infoWindow) {
            this.info = new google.maps.InfoWindow({
                maxWidth: 200,
                maxWidth: 200
            });
        }
        var mapCanvas = document.getElementById(wrapper);
        var mapOptions = {
            center           : new google.maps.LatLng(parseFloat(center.lat), parseFloat(center.lng)),
            zoom             : zoom,
            mapTypeId        : google.maps.MapTypeId.ROADMAP,
            scrollwheel      : scrollwheel,
            navigationControl: navigationControl,
            mapTypeControl   : mapTypeControl,
            disableDefaultUI : disableDefaultUI,
            minZoom          : minZoom,
            maxZoom          : maxZoom,
            scaleControl     : scaleControl
        }


        this.map = new google.maps.Map(mapCanvas, mapOptions);

        if(styles) {
            this.map.setOptions({styles: styles});
        }

        if (clickEvent) {
            google.maps.event.addListener(this.map, "click", function (event) {
                var coords = {
                    'lat': event.latLng.lat(),
                    'lng': event.latLng.lng()
                };
                that.getLocationInfo(coords)
            });
        }

        if (arrayMarkers) {
            this.drawMarkers(arrayMarkers);
        }
    }

//  Отрисовка маркеров
    this.drawMarkers = function (arrayMarkers) {
        var marker = [];
        for (var i = 0; i < arrayMarkers.length; i++) {
            marker[i] = new google.maps.Marker({
                position: new google.maps.LatLng(arrayMarkers[i][1], arrayMarkers[i][2]),
                map     : this.map,
                icon    : icon,
                title   : 'test'
            });
            if (infoWindow) {
                var data = {
                    'marker'      : marker[i],
                    'i'           : i,
                    'arrayMarkers': arrayMarkers
                }
                this.infoWindow(data)
            }
            if (infoBox) {
                var data = {
                    'appendBlock' : appendBlock,
                    'marker'      : marker[i],
                    'i'           : i,
                    'arrayMarkers': arrayMarkers
                }
                this.infoBox(data);
            }
        }

    }

//  Отрисовка попапов
    this.infoBox = function (data) {
        var appendBlock = data.appendBlock;
        var marker = data.marker;
        var i = data.i;
        var myOptions = {
            content          : appendBlock,
            disableAutoPan: false,
            maxWidth: 0,
            pixelOffset: new google.maps.Size(-140, 0),
            zIndex: null,
            boxStyle: {
                background: "background: black;color: white;",
                opacity: 1,
                width: "280px"
                },
            closeBoxMargin: "10px 2px 2px 2px",
//            closeBoxURL: '',
            infoBoxClearance: new google.maps.Size(1, 1),
            isHidden: false,
            pane: "floatPane",
            enableEventPropagation: false
        };

        that.ib = new InfoBox(myOptions);
        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
            return function () {
                that.ib.open(that.map, marker);

            }
        })(marker, i));
//        google.maps.event.addListener(marker, 'mouseout', (function (marker, i) {
//            return function () {
//                that.ib.close();
//
//            }
//        })(marker, i));
    }

//  Отрисовка попапов InfoWindow
    this.infoWindow = function (data) {
        var marker = data.marker;
        var i = data.i;
        google.maps.event.addListener(data.marker, 'click', (function (marker, i) {
            return function () {
                that.info.setContent('фывфывфы');
                that.info.open(this.map, marker);

            }
        })(marker, i));

    }
//  Поиск по адрессу
    this.searchByAddress = function (address) {
        if ($.trim(address) == 0)
            return;
        var data = {
            'address': address
        }
        that.getLocationInfo(data);

    }

//  Получение информации по координатам или по адресу
    this.getLocationInfo = function (data) {
        var latlng = new google.maps.LatLng(data.lat, data.lng);
        var request = {
            'address': data.address,
            'latLng' : latlng
        }
        geocoder = new google.maps.Geocoder();
        geocoder.geocode(request, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (typeof request.address != 'undefined') {
                    var typeZoom = results[0].types[0];
                    if (typeZoom == 'locality') {
                        zoom = 10;
                    } else if (typeZoom == 'street_address') {
                        zoom = 16
                    } else if (typeZoom == 'country') {
                        zoom = 5
                    } else {
                        zoom = 8
                    }
                    reInit = {
                        center: {
                            'lat': results[0].geometry.location.lat(),
                            'lng': results[0].geometry.location.lng()

                        },
                        zoom  : zoom
                    }
                    that.init(reInit);
                }
                var coords = {
                    'lat': results[0].geometry.location.lat(),
                    'lng': results[0].geometry.location.lng()

                }
                that.drawMarker(coords);
                var result = {
                    'status': 'success',
                    'data'  : results[0]
                }
                callback(result);
            } else {
                if (typeof that.marker != 'undefined') {
                    that.marker.setMap(null);
                }
                var result = {
                    'status': 'error',
                    'data'  : 'Ничего не найдено'
                }
                callback(result);
            }
        })

    }


//  Отрисовка маркера
    this.drawMarker = function (coords) {
//        that.getLocationInfo(coords);
        if (typeof that.marker != 'undefined') {
            that.marker.setMap(null);
        }
        this.marker = new google.maps.Marker({
            position: new google.maps.LatLng(coords.lat, coords.lng),
            map     : that.map,
            icon    : icon,
            title   : 'lol'
        });

    }

    var that = this;

    return this;
}


/**
 *
 *  @preserve Copyright 2014 Vladimir Kuzmin
 *  Version: 0.01
 *  Licensed under GPLv2.
 *  Usage: $('.class').pricetrack(options);
 *
 */
(function($){

    $.fn.pricetrack = function(options)
    {
        options = $.extend({
            track_class: '',
            drag_class: '',

            track_onchange: 0 // callback function
        }, options);

        return $(this).each(function(index, jqitem)
        {
            var that = $(jqitem);
            var i_check_drag = false;
            var i_check_start_left_css = 0;
            var i_check_start_left_page = 0;
            var i_check_track_width = that.width();

            var i_check_drag_end = function()
            {
                if (i_check_drag) {
                    i_check_drag = false;
                }
            };

            that.find('.'+options.drag_class).mousedown(function(e){
                e.preventDefault();

                if (!i_check_drag) {
                    i_check_drag = true;
                    i_check_start_left_page = e.pageX;
                    i_check_start_left_css = parseInt(that.find('.'+options.drag_class).css('left'));
                }
            });
            var previousEvent = false;
            that.mousemove(function(e){
                e.preventDefault();

                if (i_check_drag) {
                    var left_new = e.pageX - i_check_start_left_page;


                    function makeVelocityCalculator(e_init, e) {
                        var x = e_init.clientX, new_x, new_t,
                            x_dist, interval, velocity, t;
                        if (e === false) {return 0;}
                        t = e.time;
                        new_x = e.clientX;
                        new_t = Date.now();
                        x_dist = new_x - x;
                        interval = new_t - t;
                        x = new_x;
                        velocity = Math.sqrt(x_dist*x_dist)/interval;
                        return velocity;
                    }


                    e.time = Date.now();
                    var res;
                    res = makeVelocityCalculator( e, previousEvent);
                    previousEvent = e;

                    if ((i_check_start_left_css + left_new) <= that.width() && (i_check_start_left_css + left_new) >= 0) {
                        var percents = function () {
                            var position = ((i_check_start_left_css + left_new) / that.width()) * 100;
//                            console.log(res + ' - '+ position);
                            if (position < 1 && res > 0.1) {
                                return 0;
                            } else if (position > 99 && res > 0.1) {
                                return 100;
                            } else {
                                return position;
                            }





                        };

//                        console.log(percents())


                        that.find('.'+options.drag_class).css({'left' : percents() + '%' });
                        that.find('.'+options.track_class).css({'width' : percents() + '%' });

                        if (options.track_onchange !== 0) { options.track_onchange(that, percents()); }
                    }
                }

            });

            $('body').mouseup(i_check_drag_end);
            if (options.track_onchange !== 0) { options.track_onchange(that, (((parseInt(that.find('.'+options.drag_class).css('left'))) / i_check_track_width) * 100)); }
        });
    };
})(jQuery);
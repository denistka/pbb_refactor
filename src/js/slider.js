$(function () {
    sliderInit('.content-slider.content-blue_green');
    sliderInit('.content-slider.content-slider_green');
});
var flag = 0;
function sliderInit(selector) {
    var $parent = $(selector),
        $sliderContent = $parent.find('.slider-container'),
        sliderLenght = $sliderContent.length,


        curSlide = 0;

    $sliderContent.each(function (index) {
        $(this).attr({'data-sln': index});
        if (index == 0) $(this).css({'visibility': 'visible'});
        $(selector + ' .slider-nav_left').addClass('arr_not_active');
    });

    $(selector + ' .slider-nav_left').on('click', function () {
        getSlide(-1);
    });
    $(selector + ' .slider-nav_right').on('click', function () {
        getSlide(1);
    });


    function getSlide(dir) {
        console.log(flag);
        //if(timeOut) {
        //    clearTimeout(timeOut);
        //}
        if(flag == 0) {
            $(selector + ' .slider-nav').removeClass('arr_not_active');
            flag = 1;
            var nextSlide = curSlide + dir,
                pp = 2000,
                animTime = 1000,
                rAngl = 130,
                transX = 80,
                scale=0.5,
                originNext = (dir == 1) ? '100% 50%' : '0% 50%',
                originCur = (dir == 1) ? '0% 50%' : '100% 50%';

            $parent.addClass('pointer-events-off');

            ianIt($parent.find('.slider-container[data-sln="' + nextSlide + '"]'),
                {
                    'visibility': 'visible',
                    'transform-origin': originNext,
                    'transform': 'perspective(' + pp + 'px) rotateY(' + rAngl * dir + 'deg) translateX(' + dir * transX + '%) scale('+scale+')',
                    'opacity': 0
                }
            );

            ianIt($parent.find('.slider-container[data-sln="' + curSlide + '"]'),
                {
                    'transform-origin': originCur
                }
            );



            anIt($parent.find('.slider-container[data-sln="' + nextSlide + '"]'),
                {
                    'transform': 'perspective(' + pp + 'px) rotateY(0deg) translateX(0%)  scale(1)',
                    'opacity': 1
                },
                animTime, 0, ease.Out
            );

            anIt($parent.find('.slider-container[data-sln="' + curSlide + '"]'),
                {
                    'transform': 'perspective(' + pp + 'px) rotateY(' + (-1) * rAngl * dir + 'deg) translateX(' + dir * (-transX) + '%)  scale('+scale+')',
                    'opacity': 0
                },
                animTime, animTime*0.05, ease.Out, function () {
                    ianIt($parent.find('.slider-container[data-sln="' + curSlide + '"]'), {'visibility': 'hidden'});
                    curSlide = nextSlide;

                    if ((dir == 1) && (curSlide == sliderLenght - 1)) {
                        $(selector + ' .slider-nav_right').addClass('arr_not_active');
                    }
                    if ((dir == -1) && (curSlide == 0)) {
                        $(selector + ' .slider-nav_left').addClass('arr_not_active');
                    }

                    $parent.removeClass('pointer-events-off');
                }
            );

            var timeOut = setTimeout(function(){
                flag = 0;
            },1150);
        }

    }
}
$(document).ready(function(){



    $('.c-calculate_title').on('click', function() {
        $('.calculate-title-wrapper').toggleClass('calculate-title-wrapper_on')
    })

    var track_intervals_1 = [
        { val : 1000, str : '1 тыс. руб.' },
        { val : 200000, str : '200 тыс. руб.' },
        { val : 5000000, str : '5 млн. руб.' },
        { val : 25000000, str : '25 млн. руб.' },

    ];

    var track_intervals_2 = [
        { val : 2, str : '2 мес.' },
        { val : 12, str : '12 мес.' },
        { val : 60, str : '60 мес.' },
        { val : 120, str : '120 мес.' }
    ];

    var calculate_result_input = parseInt($('.c-calculate_result_input').val());
    var calculate_month_input = parseInt($('.c-calculate_month_input').val());
    var calculate_input_timer = 0;

    var previousEvent = false;

    $(document).mousemove(function(evt) {
        evt.time = Date.now();
        var res;
        res = makeVelocityCalculator( evt, previousEvent);
        previousEvent = evt;
//        console.log("velocity:"+res);
    });


    function makeVelocityCalculator(e_init, e) {
        var x = e_init.clientX, new_x,new_y,new_t,
            x_dist, y_dist, interval,velocity,
            y = e_init.clientY,
            t;
        if (e === false) {return 0;}
        t = e.time;
        new_x = e.clientX;
        new_y = e.clientY;
        new_t = Date.now();
        x_dist = new_x - x;
        y_dist = new_y - y;
        interval = new_t - t;
        // update values:
        x = new_x;
        y = new_y;
        velocity = Math.sqrt(x_dist*x_dist+y_dist*y_dist)/interval;
        return velocity;
    }

    $('.c-calculate_result_input').on('input', function(e){
        var that = $(this);
        var that_val = parseInt(that.val());
        clearTimeout(calculate_input_timer);




        if (that_val > 0 && !isNaN(that.val()) && track_intervals_1[0].val <= that_val && that_val <= track_intervals_1[track_intervals_1.length-1].val) {
            calculate_result_input = parseInt(that.val());

            var i = -1;
            for (var j = 0; j < track_intervals_1.length - 1; j++) {
                if (track_intervals_1[j].val <= calculate_result_input && calculate_result_input <= track_intervals_1[j+1].val) {
                    i = j;
                    break;
                }
            }
            if (i > -1) {
                var track_number = calculate_result_input - track_intervals_1[i].val;
                var track_part = (track_intervals_1[i+1].val - track_intervals_1[i].val) / 100;
                var interval_percents = track_number / track_part;
                var real_percents = Math.max(0, Math.min(100, (interval_percents / 3 + i * 33.3333)));





                $('.c-trackbar_1 .c-trackbar_drag').css({'left' : real_percents + '%' });
                $('.c-trackbar_1 .c-trackbar_track_fill').css({'width' : real_percents + '%' });
            }
        } else {
            calculate_input_timer = setTimeout(function(){
                that.val(calculate_result_input);
            }, 500);
        }
    });

    $('.c-calculate_month_input').on('input', function(e){
        var that = $(this);
        var that_val = parseInt(that.val());
        clearTimeout(calculate_input_timer);

        if (that_val > 0 && !isNaN(that.val()) && track_intervals_2[0].val <= that_val && that_val <= track_intervals_2[track_intervals_2.length-1].val) {
            calculate_month_input = parseInt(that.val());

            var i = -1;
            for (var j = 0; j < track_intervals_2.length - 1; j++) {
                if (track_intervals_2[j].val <= calculate_month_input && calculate_month_input <= track_intervals_2[j+1].val) {
                    i = j;
                    break;
                }
            }
            if (i > -1) {
                var track_number = calculate_month_input - track_intervals_2[i].val;
                var track_part = (track_intervals_2[i+1].val - track_intervals_2[i].val) / 100;
                var interval_percents = track_number / track_part;
                var real_percents = Math.max(0, Math.min(100, (interval_percents / 3 + i * 33.3333)));

                $('.c-trackbar_2 .c-trackbar_drag').css({'left' : real_percents + '%' });
                $('.c-trackbar_2 .c-trackbar_track_fill').css({'width' : real_percents + '%' });
            }
        } else {
            calculate_month_input = setTimeout(function(){
                that.val(calculate_month_input);
            }, 500);
        }
    });

    $('.c-trackbar_1').pricetrack({
        track_class : 'c-trackbar_track_fill',
        drag_class : 'c-trackbar_drag',
        track_onchange : function(that, percents) {
            that.find('.c-trackbar_table_item').each(function(idx, jqitem){
                if (idx < track_intervals_1.length) {
                    $(jqitem).html(track_intervals_1[idx].str);
                }
            });
            percents = parseFloat(percents);
            var track_percent_part = 100 / 3;
            var i = Math.floor(percents / track_percent_part);
            if (i > track_intervals_1.length - 2) { i = track_intervals_1.length - 2; }
            var interval_percents = (percents % track_percent_part) * 3;
            var track_part = (track_intervals_1[i+1].val - track_intervals_1[i].val) / 100;
            var track_value = 1000 * Math.floor(Math.ceil(track_intervals_1[i].val + interval_percents * track_part) / 1000);


            $('.c-calculate_result_input').val(track_value);

            calculate_result_input = parseInt($('.c-calculate_result_input').val());
        }
    });

    $('.c-trackbar_2').pricetrack({
        track_class : 'c-trackbar_track_fill',
        drag_class : 'c-trackbar_drag',
        track_onchange : function(that, percents) {
            that.find('.c-trackbar_table_item').each(function(idx, jqitem){
                if (idx < track_intervals_2.length) {
                    $(jqitem).html(track_intervals_2[idx].str);
                }
            });

            percents = parseFloat(percents);
            var track_percent_part = 100 / 3;
            var i = Math.floor(percents / track_percent_part);
            if (i > track_intervals_2.length - 2) { i = track_intervals_2.length - 2; }
            var interval_percents = (percents % track_percent_part) * 3;
            var track_part = (track_intervals_2[i+1].val - track_intervals_2[i].val) / 100;
            var track_value = Math.ceil(track_intervals_2[i].val + interval_percents * track_part);

            $('.c-calculate_result_text input').val(track_value);
        }
    });

});

$(function () {


   //$('.content-rko-two .table-wrap .right').jScrollPane({
   //    showArrows: false,
   //    animateScroll: false,
   //    mouseWheelSpeed: 30,
   //    autoReinitialise: true,
   //    //autoReinitialiseDelay: 100,
   //    hideFocus: true,
   //    horizontalDragMaxWidth: 117,
   //    horizontalDragMixWidth: 117,
   //    contentHeight: '0px'
   //});

    $(window).load(function(){
        $('.content-rko-two .table-wrap .table-content').css({ width : $('.content-rko-two .table-wrap .table-content .tcol-width').width() * $('.content-rko-two .table-wrap .table-content .tcol-width').length  });
        setTimeout(function(){
            $(".right.horizontal-only").mCustomScrollbar({
                axis:"x",
                autoDraggerLength: false,
                scrollInertia: 0
            });
        }, 20);
    });
});

$('.tarifs-info').hover(function (e) {
    var info = $(this);
    e.stopPropagation();
    info.parent().toggleClass('rate-name_active');

    // timeout to prevent fast change for popup window
    setTimeout(function () {
        if (info.parent().hasClass('rate-name_active')) {
            info.parent().addClass('rate-name_active_show')
        } else {
            info.parent().removeClass('rate-name_active_show')
        }
    }, 300);


});


$("input.js_telephone").mask("(999) 999 - 99 - 99", {placeholder: " "});

$(document).on('click', '.btn_tarifAndDocs', function(){

    var className = $(this).attr('data');
    var block = $('.tarifAndDocs.' + className);
    if(!block.is(':visible')) {
        block.slideDown();
    } else {
        block.slideUp();
    }

    return false;

}).on('click', '.tarifAndDocs .close', function(){

    var block = $(this).parents('.tarifAndDocs');
    block.slideUp();

});
$(function () {
    if ($('.b-main-nav').length) {
     mainMenuDropDown();
    }
});

var hoverTimeout = null;
var topMenuOffset=0;

function mainMenuDropDown() {
    $('body').on('click', function (e) {
        var menu = $('.b-main-nav');
        if (e.target != menu.get(0) && $(e.target).parents('.b-main-nav').length == 0) {
            flipMenuLevelClose('.level-three', function () {
                flipMenuLevelClose('.level-two');
            });
        }
    });

    $(window).on('scroll',function(){
        flipMenuLevelClose('.level-three', function () {
            flipMenuLevelClose('.level-two');
        });

        var $menu=$('.b-main-nav'),
            scrollTop=$(window).scrollTop(),
            menuOffset=$menu.position();


        if ((scrollTop>=menuOffset.top)&&(!$menu.hasClass('fixed-position'))) {
            $menu.addClass('fixed-position');
            topMenuOffset=menuOffset.top;
        }
        else if ((scrollTop<topMenuOffset)&&($menu.hasClass('fixed-position'))){
            $menu.removeClass('fixed-position');
            topMenuOffset=0;
        }
    });

    $('.b-main-nav .top-part .b-main-nav__arrow')
        .on('click', function (e) {
            e.stopPropagation();
            var self = this;

            // меню закрыто - открываем
            if (!$(self).hasClass('nav_active')) {
                flipMenuLevelOpen('.level-two');
            }
            // меню открыто - закрываем
            else {
                flipMenuLevelClose('.level-two');
            }
        });

    $('.b-main-nav .top-part .b-main-nav__list li a')
        .not('.not-drop-down')
        .on('click', function (e) {
            e.stopPropagation();
            var self = this;

            $('.b-main-nav .top-part .b-main-nav__list li a.active').removeClass('active');
            $(self).addClass('active');

            flipMenuLevelOpen('.level-two');
        });

    /*$('.b-main-nav .dropdownpart.level-two .b-main-nav__list li a')
        .not('.dropdownpart.level-three .b-main-nav__list li a')
        .on('click', function (e) {
            e.stopPropagation();
            clearTimeout(hoverTimeout);
            hoverTimeout = setTimeout(function () {
                flipMenuLevelOpen('.level-three');
            }, 200);
        });*/

    function flipMenuLevelOpen(levelSelector) {
        var openDelay = 0;
        if ($('.b-main-nav .dropdownpart' + levelSelector).hasClass('open')) {
            anIt('.b-main-nav .dropdownpart' + levelSelector, {transform: 'perspective(3000px) rotateX(-90deg)'}, 500, 0, ease.Out);
            openDelay = 300;
        }

        $('.b-main-nav__arrow').addClass('nav_active');
        $('.b-main-nav').addClass('dropdown_opened');

        ianIt('.b-main-nav .dropdownpart' + levelSelector, {'visibility': 'visible'});
        $('.b-main-nav .dropdownpart' + levelSelector).addClass('open');
        anIt('.b-main-nav .dropdownpart' + levelSelector, {transform: 'perspective(3000px) rotateX(0deg)'}, 500, openDelay, ease.Out);
    }

    function flipMenuLevelClose(levelSelector, _cb) {
        var cb = (typeof _cb !== 'undefined') ? _cb : $.noop;

        if (!$('.b-main-nav .dropdownpart' + levelSelector).hasClass('open')) {
            cb();
            return;
        }

        anIt('.b-main-nav .dropdownpart' + levelSelector, {transform: 'perspective(3000px) rotateX(-90deg)'}, 300, 0, ease.In, function () {
            $('.b-main-nav .dropdownpart' + levelSelector).removeClass('open');
            ianIt('.b-main-nav .dropdownpart' + levelSelector, {'visibility': 'hidden'});
            cb();
        });

        $('.b-main-nav__arrow').removeClass('nav_active');
        $('.b-main-nav').removeClass('dropdown_opened');

        $('.b-main-nav .top-part .top-part .b-main-nav__list li a.active').removeClass('active');
    }
}
